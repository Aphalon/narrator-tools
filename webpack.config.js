const webpack = require('webpack')
const path = require('path')

const NI = new webpack.DefinePlugin({
  INTERFACE: JSON.stringify('NI'),
  CURRENT_MAP_ID: 'Engine.map.d.id'
})
const SI = new webpack.DefinePlugin({
  INTERFACE: JSON.stringify('SI'),
  CURRENT_MAP_ID: 'map.id'
})

const constants = new webpack.DefinePlugin({
  NERTHUS_FILE_PREFIX: JSON.stringify('https://raw.githubusercontent.com/nerthus-margonem/nerthusaddon/production/')
})

module.exports = [
  {
    name: 'NI_production',
    mode: 'production',
    entry: './src/init-ni.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'mainNI.js'
    },
    plugins: [
      constants,
      NI
    ]
  },
  {
    name: 'SI_production',
    mode: 'production',
    entry: './src/init-si.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'mainSI.js'
    },
    plugins: [
      constants,
      SI
    ]
  }
]
