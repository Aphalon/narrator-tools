# Narrator tools
Add-on for [Margonem](https://www.margonem.pl/) allowing easy World Edit with nice UI for inexperienced admins.

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard%20with%20allman%20braces-brightgreen.svg)](https://www.npmjs.com/package/standardx)

### Options so far:
* Adding and deleting new NPCs
* Saving custom NPCs for later use
* Changing map image, with ability to save images for later use
* Setting weather
* Checking list of online narrators
### Advanced:
* Loading add-on to set lights on map
* Turning on and off night lights
* Saving NPCs on map to file format
* Editing NPCs dialogue

### Add-on settings:
* Changing position on screen
* Changing between two styles (horizontal and vertical)
* Changing type of adding NPCs (click on image, click on map or click on image, edit it's settings and then click 'add')
