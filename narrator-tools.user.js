// ==UserScript==
// @name         Nerthus - Narrator Tools
// @namespace    http://www.margonem.pl/
// @version      5.3.0
// @description  Addon for Nerthus's narrators
// @author       Kris Aphalon
// @match        http://nerthus.margonem.pl/
// @match        https://nerthus.margonem.pl/
// @run-at       document-idle
// @grant        none
// ==/UserScript==

(function ()
{
  function logToConsole (msg, gameInterface, error = false)
  {
    if (gameInterface === 'ni' && typeof window.Engine.console === 'undefined')
    {
      return setTimeout(() => logToConsole(msg, gameInterface), 500)
    }
    if (error) return window.error(msg)
    return window.log(msg)
  }

  function start ()
  {
    const arr = /interface=(..)/.exec(document.cookie)
    if (arr)
    {
      const gameInterface = arr[1]
      let src
      if (gameInterface === 'ni') src = 'https://glcdn.githack.com/Aphalon/narrator-tools/-/raw/production/dist/mainNI.js'
      else if (gameInterface === 'si') src = 'https://glcdn.githack.com/Aphalon/narrator-tools/-/raw/production/dist/mainSI.js'
      else
      {
        const errorMsg =
          'Narrator tools addon couldn\'t detect your interface. ' +
          'Try restarting your game or cleaning cookies. ' +
          'If this error persists, submit a bug to the developer.'
        console.error(errorMsg)
        logToConsole(errorMsg, gameInterface, true)
      }

      if (src)
      {
        let logText = 'Narrator tools version: 5.3.0'
        if (gameInterface === 'si') logText = '<span style="color:lime">' + logText + '</span>'
        const script = document.createElement('script')
        script.src = src
        document.head.appendChild(script)
        logToConsole(logText, gameInterface, false)
      }
    }
    else setTimeout(start, 500)
  }

  start()
})()
