import { afterNerthusDefined } from './helper-functions'
import { addFunctionToChatParser, initChatParser } from './chat-parser'

window.narratorTools = {}
window.narratorTools.interface = 'SI'
const graphics = require('./SI_specific/map-objects-manipulation')
window.narratorTools.sharedElements = {
  botloc: document.getElementById('botloc'),
  gameWindow: document.getElementById('base')
}
Object.defineProperty(window.narratorTools.sharedElements, 'mapId', {
  get ()
  {
    return window.map.id
  }
})
window.narratorTools.sharedFunctions = {
  addToChatParser: addFunctionToChatParser,
  getMousePosition (e)
  {
    const ground = document.getElementById('ground').getBoundingClientRect()

    const position = {}
    position.x = ((e.clientX - ground.x) >> 5)
    position.y = ((e.clientY - ground.y) >> 5)

    return position
  },
  addLock (name)
  {
    window.g.lock.add(name)
  },
  removeLock (name)
  {
    window.g.lock.remove(name)
  },
  paintSquare: graphics.paintSquare,
  removeSquare: graphics.removeSquare,
  getObjectUnderMouse: graphics.getObjectUnderMouse,
  underlayNPCs: graphics.underlayNPCs,
  endUnderlayNPCs: graphics.endUnderlayNPCs,
  getNpcData (id)
  {
    return window.g.npc[id]
  },
  getHeroNick ()
  {
    return window.hero.nick
  },
  sendChatMsg (msg)
  {
    let channelName = window.g.chatController.getChatInputWrapper().getChannelName().toLowerCase()
    if (!['group', 'local'].includes(channelName))
    {
      channelName = 'local'
    }
    if (channelName === 'group')
    {
      channelName = 'party'
    }
    window._g(`chat&channel=${channelName}`, false, { c: msg })
  }
}

initChatParser()
afterNerthusDefined(function ()
{
  window.narratorTools.sharedElements.widget = document.getElementById('nerthus-shield')
  require('./main.js').start()
})
