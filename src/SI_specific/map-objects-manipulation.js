const coordHelper = document.createElement('coordHelper')
const npcSearchStyle = document.createElement('style')

export function paintSquare (x, y)
{
  coordHelper.style.left = Number(x) * 32 + 'px'
  coordHelper.style.top = Number(y) * 32 + 'px'
  coordHelper.style.visibility = 'visible'

  const ground = document.getElementById('ground')
  if (!ground.contains(coordHelper))
  {
    coordHelper.id = 'coordHelper'
    ground.appendChild(coordHelper)
  }
}

export function removeSquare ()
{
  coordHelper.style.visibility = 'hidden'
}

/**
 * @param e {MouseEvent}
 */
export function getObjectUnderMouse (e)
{
  const npc = {}

  if (e.target.classList.contains('nerthus-npc'))
  {
    const id = Number(e.target.id.substring(3))
    if (id > 5000000000)
    {
      npc.x = Math.floor((id - 5000000000) / 100000)
      npc.y = id - 5000000000 - (npc.x * 100000)
    }
    else
    {
      npc.x = Math.floor((id - 50000000) / 1000)
      npc.y = id - 50000000 - (npc.x * 1000)
    }
    npc.type = 'custom'
    npc.id = false
    npc.icon = e.target.style.backgroundImage.substring(5, e.target.style.backgroundImage.length - 2)
    const tip = e.target.attributes.getNamedItem('tip')
    if (tip)
    {
      npc.nick = tip.value.replace(/(<b>)|(<\/b>)/g, '')
    }
    else
    {
      npc.nick = ''
    }
    npc.collision = window.isCollision(npc.x, npc.y)

    return npc
  }

  const regexDefault = /npc(.*)/g
  const matchDefault = regexDefault.exec(e.target.id)
  if (!matchDefault)
  {
    return false
  }
  npc.type = 'default'
  npc.id = matchDefault[1]
  const gameNpc = window.g.npc[npc.id]

  npc.x = gameNpc.x
  npc.y = gameNpc.y
  npc.icon = gameNpc.icon
  if (gameNpc.type === 4)
  {
    npc.nick = ''
  }
  else
  {
    npc.nick = gameNpc.nick
  }
  npc.collision = window.isCollision(npc.x, npc.y)

  return npc
}

export function underlayNPCs (color)
{
  npcSearchStyle.innerText = '#base div[id^="npc"], #base img[id^="_ng-"]{background-color: ' + color + ';pointer-events:auto!important}'
  if (npcSearchStyle.parentNode !== document.head)
  {
    document.head.appendChild(npcSearchStyle)
  }
}

export function endUnderlayNPCs ()
{
  if (npcSearchStyle.parentNode === document.head)
  {
    document.head.removeChild(npcSearchStyle)
  }
}
