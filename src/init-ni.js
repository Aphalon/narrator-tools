import { afterNerthusDefined } from './helper-functions'
import { addFunctionToChatParser, initChatParser } from './chat-parser'

window.narratorTools = {}
window.narratorTools.interface = 'NI'
const graphics = require('./NI_specific/map-objects-manipulation')
window.narratorTools.sharedElements = {
  botloc: document.getElementsByClassName('world-name')[0],
  gameWindow: document.getElementsByClassName('game-layer')[0]
}
Object.defineProperty(window.narratorTools.sharedElements, 'mapId', {
  get ()
  {
    return window.Engine.map.d.id
  }
})
window.narratorTools.sharedFunctions = {
  addToChatParser (parser)
  {
    addFunctionToChatParser(function (data)
    {
      parser(data)
    })
  },
  getMousePosition (e)
  {
    const factor = window.Engine.zoomFactor
    const clientX = e.clientX / factor
    const clientY = e.clientY / factor

    const rect = document.getElementById('GAME_CANVAS').getBoundingClientRect()

    const leftOffset = rect.left + document.body.scrollLeft
    const topOffset = rect.top + document.body.scrollTop

    const position = {}
    position.x = Math.trunc(clientX - leftOffset / factor + window.Engine.map.offset[0]) >> 5
    position.y = Math.trunc(clientY - topOffset / factor + window.Engine.map.offset[1]) >> 5

    return position
  },
  addLock (name)
  {
    window.Engine.lock.add(name)
  },
  removeLock (name)
  {
    window.Engine.lock.remove(name)
  },
  paintSquare: graphics.paintSquare,
  removeSquare: graphics.removeSquare,
  getObjectUnderMouse: graphics.getObjectUnderMouse,
  underlayNPCs: graphics.underlayNPCs,
  endUnderlayNPCs: graphics.endUnderlayNPCs,
  getNpcData (id)
  {
    const npc = window.Engine.npcs.getById(id)
    if (npc)
    {
      return npc.d
    }
  },
  getHeroNick ()
  {
    return window.Engine.hero.nick
  },
  sendChatMsg (msg)
  {
    let channelName = window.Engine.chatController.getChatInputWrapper().getChannelName().toLowerCase()
    if (!['group', 'local'].includes(channelName))
    {
      channelName = 'local'
    }
    if (channelName === 'group')
    {
      channelName = 'party'
    }
    window._g(`chat&channel=${channelName}`, false, { c: msg })
  }
}

initChatParser()
afterNerthusDefined(function ()
{
  window.narratorTools.sharedElements.widget = document.getElementsByClassName('widget-nerthus')[0]
  require('./main.js').start()
})
