import { saveSettings, settings } from './settings'
import { frameElement } from './main-panel'

let pos1
let pos2
let pos3
let pos4

// const allowedDragElements = []
let currentDragElement
let dragElementStartMarginTop
let dragElementStartMarginLeft

export function dragMouseDown (event)
{
  if (event.target !== event.currentTarget)
  {
    return
  }
  window.narratorTools.sharedFunctions.addLock('narratorTools-dragging')
  event.preventDefault()

  // get the mouse cursor position at startup:
  pos3 = event.clientX
  pos4 = event.clientY
  currentDragElement = event.target
  const styles = getComputedStyle(currentDragElement)
  dragElementStartMarginTop = parseInt(styles.marginTop)
  dragElementStartMarginLeft = parseInt(styles.marginLeft)
  document.addEventListener('mousemove', elementDrag)
  document.addEventListener('mouseup', closeDragElement)
}

export function setDraggable (element)
{
  element.addEventListener('mousedown', dragMouseDown, false)
}

export function revokeDraggable (element)
{
  element.removeEventListener('mousedown', dragMouseDown, false)
}

function elementDrag (event)
{
  event.preventDefault()
  // calculate the new cursor position:
  pos1 = pos3 - event.clientX
  pos2 = pos4 - event.clientY
  pos3 = event.clientX
  pos4 = event.clientY
  // set the element's new position:
  currentDragElement.style.top = (currentDragElement.offsetTop - pos2 - dragElementStartMarginTop) + 'px'
  currentDragElement.style.left = (currentDragElement.offsetLeft - pos1 - dragElementStartMarginLeft) + 'px'
}

function closeDragElement ()
{
  // save main frame position
  // TODO move it somewhere
  settings.position.top = frameElement.style.top
  settings.position.left = frameElement.style.left
  saveSettings()

  window.narratorTools.sharedFunctions.removeLock('narratorTools-dragging')
  document.removeEventListener('mousemove', elementDrag)
  document.removeEventListener('mouseup', closeDragElement)
}
