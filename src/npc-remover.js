import { removeNpc } from './chat'
import { setTip, setTipType } from './tips'

const npcList = []
export const deleteNpcContainer = document.createElement('div')

/**
 * @function
 * @param {MouseEvent} e - event that has target as one of the deleteNpcContainer npc buttons
 * @returns {HTMLElement} - npc on location map
 */
function getNpcFromEvent (e)
{
  const idArr = (e.target.id).split('-')
  const x = idArr[1]
  const y = idArr[2]
  return document.getElementById('_ng-' + x + '-' + y)
}

function highlightDeleteNpc (e)
{
  if (INTERFACE === 'SI')
  {
    const npc = getNpcFromEvent(e)
    if (npc)
    {
      npc.style.outline = '#CD0000 solid 3px'
    }
  }
  else
  {
    console.log('WIP')
  }
}

function endHighlightDeleteNpc (e)
{
  if (window.narratorTools.interface === 'SI')
  {
    const npc = getNpcFromEvent(e)
    if (npc)
    {
      npc.style.outline = ''
    }
  }
  else
  {
    console.log('WIP')
  }
}

export function addButtonToDeleteNpc (data)
{
  const { npc, mapId } = data
  const tip = '<b>' + npc.nick + '</b><span style="text-align: center">' + npc.x + ',' + npc.y + '</span>'
  const newNpc = document.createElement('img')
  newNpc.id = '_ng-' + npc.x + '-' + npc.y + '-addGraf'
  newNpc.src = npc.icon

  setTip(newNpc, tip)
  setTipType(newNpc, 't_npc')

  newNpc.addEventListener('click', removeNpc.bind(this, npc.x, npc.y, mapId))

  npcList.push({ x: npc.x, y: npc.y, mapId: mapId, elm: newNpc })
  deleteNpcContainer.appendChild(newNpc)
}

function deleteNpcHandler (data)
{
  const { x, y, mapId } = data
  for (let i = 0; i < npcList.length; i++)
  {
    if (npcList[i].x === x && npcList[i].y === y && npcList[i].mapId === mapId)
    {
      npcList[i].elm.parentNode.removeChild(npcList[i].elm)
      npcList.splice(i, 1)
    }
  }
}

export function initDeleteBox ()
{
  nerthus.addCallbackToEvent('addTemporaryNpc', addButtonToDeleteNpc)
  nerthus.addCallbackToEvent('removeTemporaryNpc', deleteNpcHandler)

  deleteNpcContainer.addEventListener('mouseover', highlightDeleteNpc, false)
  deleteNpcContainer.addEventListener('mouseleave', endHighlightDeleteNpc, true)
}
