/**
 *
 * @package
 * @author Kris Aphalon
 * @type {Array}
 */

const checkList = []
const previousCommands = []

export function addToChatChecklist (command, handler)
{
  checkList.push([command, handler])
  for (const ch of previousCommands)
  {
    const commands = ch.s.split(' ')
    for (const cmd of commands)
    {
      if (cmd === command)
      {
        handler(ch.t)
      }
    }
  }
}

export function addNpc (x, y, url, name, collision)
{
  if (url.startsWith('https://cdn.discordapp.com'))
  {
    url = url.replace('https://cdn.discordapp.com', 'https://media.discordapp.net')
  }

  window.narratorTools.sharedFunctions.sendChatMsg(
    `*addGraf ${x},${y},${url},${name},${collision},${window.narratorTools.sharedElements.mapId}`
  )
  return true
}

export function removeNpc (x, y, mapName)
{
  console.log(mapName)
  if (typeof mapName === 'undefined')
  {
    window.narratorTools.sharedFunctions.sendChatMsg(
      '*delGraf ' + x + ',' + y
    )
  }
  else
  {
    window.narratorTools.sharedFunctions.sendChatMsg(
      '*delGraf ' + x + ',' + y + ',' + mapName
    )
  }
  return true
}

export function changeMap (src)
{
  if (src.startsWith('https://cdn.discordapp.com'))
  {
    src = src.replace('https://cdn.discordapp.com', 'https://media.discordapp.net')
  }

  window.narratorTools.sharedFunctions.sendChatMsg(
    '*map ' + encodeURI(src) + ',' + window.narratorTools.sharedElements.mapId
  )
}

export function resetMap ()
{
  window.narratorTools.sharedFunctions.sendChatMsg(
    '*resetMap' + ' ' + window.narratorTools.sharedElements.mapId
  )
}

export function changeLight (value, color)
{
  window.narratorTools.sharedFunctions.sendChatMsg(
    `*light ${value},${color}`
  )
  // _g("chat', { c: '*light " + lightValue + "," + map.id);//TODO change to that when you can add map on 1 map only
}

export function resetLight ()
{
  window.narratorTools.sharedFunctions.sendChatMsg(
    '*light'
  )
}

export function changeWeather (weatherName)
{
  window.narratorTools.sharedFunctions.sendChatMsg(`*weather ${weatherName}, ${CURRENT_MAP_ID}`)
}

export function hideNpc (id)
{
  window.narratorTools.sharedFunctions.sendChatMsg(
    '*hide ' + id
  )
}

export function initChat ()
{
  // window.narratorTools.sharedFunctions.addToChatParser(chatCheck)
}
