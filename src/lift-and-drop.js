import { addNpc } from './chat'

let buttonToHighlight
let isNpcAttachedToMouse = false
const attachedNpc = {
  x: 0,
  y: 0,
  url: '',
  name: '',
  collision: false,
  element: document.createElement('div'),
  image: document.createElement('img')
}
const ground = window.narratorTools.interface === 'SI' ? document.getElementById('ground') : document.getElementsByClassName('game-layer')[0]

function moveAttached (e)
{
  let x = 0
  let y = 0

  if (window.narratorTools.interface === 'SI')
  {
    const groundBounding = ground.getBoundingClientRect()
    x = (e.clientX - groundBounding.x) >> 5
    y = (e.clientY - groundBounding.y) >> 5
    attachedNpc.element.style.left = x * 32 + 'px'
    attachedNpc.element.style.top = y * 32 + 'px'
  }
  else
  {
    const gameCanvas = document.getElementById('GAME_CANVAS')
    const factor = window.Engine.zoomFactor
    const clientX = e.clientX / factor
    const clientY = e.clientY / factor

    const rect = gameCanvas.getBoundingClientRect()

    const leftOffset = rect.left + document.body.scrollLeft
    const topOffset = rect.top + document.body.scrollTop

    x = Math.trunc(clientX - leftOffset / factor + window.Engine.map.offset[0]) >> 5
    y = Math.trunc(clientY - topOffset / factor + window.Engine.map.offset[1]) >> 5

    attachedNpc.element.style.left = x * 32 - window.Engine.map.offset[0] + 'px'
    attachedNpc.element.style.top = y * 32 - window.Engine.map.offset[1] + 'px'
  }
  attachedNpc.element.style.zIndex = y * 2 + 9
  attachedNpc.x = x
  attachedNpc.y = y
}

function clearAttached (e)
{
  isNpcAttachedToMouse = false
  if (e)
  {
    e.preventDefault()
  }

  attachedNpc.element.style.display = 'none'
  window.removeEventListener('mousemove', moveAttached, true)
  window.removeEventListener('contextmenu', clearAttached)
  if (window.narratorTools.interface === 'SI')
  {
    document.getElementById('centerbox')
      .removeEventListener('click', setAttached, true)
  }
  else
  {
    document.getElementById('GAME_CANVAS')
      .removeEventListener('click', setAttached, true)
  }

  if (buttonToHighlight)
  {
    buttonToHighlight.classList.remove('active')
  }
}

function setAttached (e)
{
  window.narratorTools.sharedFunctions.addLock('coordSearch')
  // let ground = document.getElementById("ground").getBoundingClientRect();
  // let x = (e.clientX - ground.x) >> 5;
  // let y = (e.clientY - ground.y) >> 5;
  clearAttached(e)
  // let npc = attachedNPC;

  // sets focus out of input boxes and allows immediate mouse movement
  document.body.focus()

  return addNpc(
    attachedNpc.x,
    attachedNpc.y,
    attachedNpc.url,
    attachedNpc.name,
    attachedNpc.collision ? 1 : 0
  )
}

/**
 *
 * @param url - url to image of npc
 * @param name - name of npc to place
 * @param collision - whether placed npc should have collision
 * @param confirmButton - button to highlight
 */
export function attachNpcToMouse (url, name, collision, confirmButton)
{
  if (confirmButton)
  {
    buttonToHighlight = confirmButton
    buttonToHighlight.classList.add('active')
  }
  isNpcAttachedToMouse = true
  console.log(collision)
  attachedNpc.url = url
  attachedNpc.name = name
  if (typeof collision === 'undefined')
  {
    attachedNpc.collision = false
  }
  else
  {
    attachedNpc.collision = collision
  }

  attachedNpc.image.src = url
  if (attachedNpc.element.style.display !== 'block')
  {
    attachedNpc.element.style.display = 'block'
    attachedNpc.element.style.left = '-32px'
    attachedNpc.element.style.top = '-32px'
    attachedNpc.element.style.zIndex = '1'
    window.addEventListener('mousemove', moveAttached, true)
    window.addEventListener('contextmenu', clearAttached)
    if (window.narratorTools.interface === 'SI')
    {
      document.getElementById('centerbox')
        .addEventListener('click', setAttached, true)
    }
    else
    {
      document.getElementById('GAME_CANVAS')
        .addEventListener('click', setAttached, true)
    }

    if (attachedNpc.element.parentElement === null)
    {
      attachedNpc.element.id = 'NPC-position-helper'
      attachedNpc.image.id = 'NPC-position-helper-img'
      attachedNpc.element.appendChild(attachedNpc.image)
      ground.appendChild(attachedNpc.element)
    }
  }
}

export function updateAttachedNpcInformation (url, name, collision)
{
  console.log(collision)
  if (isNpcAttachedToMouse)
  {
    attachedNpc.url = url
    attachedNpc.name = name
    if (typeof collision === 'undefined')
    {
      attachedNpc.collision = false
    }
    else
    {
      attachedNpc.collision = collision
    }

    attachedNpc.image.src = url
  }
}
