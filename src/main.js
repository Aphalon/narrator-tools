import { displayModule, drawMainFrame, frameElement, toggleMainFrameVisibility } from './main-panel'
import { initChat } from './chat'
import { addAfterLoadSettingsCallback, loadSettings, saveSettings, settings } from './settings'
import { loadStyle } from './style'

import { dragMouseDown } from './dragging'
import { initLogSaver } from './log-saver'

export function start ()
{
  console.log('start! ^^')
  console.log(settings)

  function firstUse ()
  {
    if (window.screen.height > 800)
    {
      settings.style = 'horizontal'
    }
    else
    {
      settings.style = 'vertical'
    }
    saveSettings()
  }

  /* Start */
  function initiate ()
  {
    addAfterLoadSettingsCallback(function ()
    {
      frameElement.style.left = settings.position.left
      frameElement.style.top = settings.position.top
    })

    if (window.localStorage.getItem('narratorToolsSettings') !== null) loadSettings()
    else firstUse()

    loadStyle(settings.style)

    initLogSaver()
    initChat()

    window.narratorTools.sharedElements.widget.addEventListener('contextmenu', toggleMainFrameVisibility)

    if (settings.visible)
    {
      drawMainFrame()
      displayModule(settings.lastVisited)
    }

    if (settings.draggable)
    {
      frameElement.addEventListener('mousedown', dragMouseDown, false)
    }

    window.narratorTools.resetPosition = function ()
    {
      settings.position.left = ''
      settings.position.top = ''

      frameElement.style.left = settings.position.left
      frameElement.style.top = settings.position.top

      saveSettings()
    }
  }

  initiate()
}
