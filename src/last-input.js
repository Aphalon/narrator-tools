export default new function ()
{
  this.save = function (name, inputFrame)
  {
    const data = {}

    const len = inputFrame.children.length
    for (let i = 0; i < len; i++)
    {
      if (inputFrame.children[i].tagName.toLowerCase() === 'input')
      {
        if (inputFrame.children[i].type === 'checkbox')
        {
          data[inputFrame.children[i].id] = inputFrame.children[i].checked
        }
        else
        {
          data[inputFrame.children[i].id] = inputFrame.children[i].value
        }
      }
    }

    window.localStorage.setItem('narratorTools-lastInput-' + name, JSON.stringify(data))
  }
  this.load = function (name, inputFrame)
  {
    const rawData = window.localStorage.getItem('narratorTools-lastInput-' + name)
    if (rawData)
    {
      const data = JSON.parse(rawData)
      if (data !== null)
      {
        console.log(data)
        const len = inputFrame.children.length
        for (let i = 0; i < len; i++)
        {
          if (inputFrame.children[i].tagName.toLowerCase() === 'input')
          {
            if (inputFrame.children[i].type === 'checkbox')
            {
              inputFrame.children[i].checked = data[inputFrame.children[i].id]
            }
            else
            {
              inputFrame.children[i].value = data[inputFrame.children[i].id]
            }
          }
        }
      }
      return data
    }
    return false
  }
  this.reset = function (name, inputFrame)
  {
    window.localStorage.removeItem('narratorTools-lastInput-' + name)

    const len = inputFrame.children.length
    for (let i = 0; i < len; i++)
    {
      if (inputFrame.children[i].tagName.toLowerCase() === 'input')
      {
        if (inputFrame.children[i].type === 'checkbox')
        {
          inputFrame.children[i].checked = false
        }
        else
        {
          inputFrame.children[i].value = ''
        }
      }
    }
  }
}()
