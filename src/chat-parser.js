import { settings } from './settings'

const chatParserFunctionList = []
const parsedMessages = []

// TODO: add support to faster map switching on SI

/**
 * @param func
 */
export function addFunctionToChatParser (func)
{
  for (const msg of parsedMessages)
  {
    func(msg)
  }
  chatParserFunctionList.push(func)
}

function getCommandFromMsg (msg)
{
  switch (msg.style)
  {
    case 2:
      return 'me'
    case 7:
      return 'entertown'
    case 'nerthus-command':
      return 'command ' + msg.text.split(' ')[0].substring(1)
    case 'nerthus-nar0':
      return 'nar nar0'
    case 'nerthus-nar1':
      return 'nar nar1'
    case 'nerthus-nar2':
      return 'nar nar2'
    case 'nerthus-nar3':
      return 'nar nar3'
    case 'nerthus-nar-rainbow':
      return 'nar nar6'
    case 'nerthus-dial1':
      return 'dial dial1'
    case 'nerthus-dial2':
      return 'dial dial2'
    case 'nerthus-dial3':
      return 'dial dial3'
    case 'nerthus-dial666':
      return 'dial dial666'
    case 'nerthus-sys':
      return 'sys'
    default:
      if (typeof msg.style === 'string' && msg.style.includes('nerthus-lang'))
      {
        const match = /`nerthus-lang nerthus-lang-ts-(.*)`/.exec(msg.style)
        if (match)
        {
          return match[1]
        }
      }
      return ''
  }
}

function getTextFromMessage (msg)
{
  msg.text = msg.text.replace('https*Krzywi się.*/', 'https://')

  if (['nerthus-dial1', 'nerthus-dial2', 'nerthus-dial3', 'nerthus-dial666'].includes(msg.style))
  {
    return (msg.authorBusinessCard?.getNick() ?? '') + ',' + msg.text
  }
  return msg.text
}

/**
 * Run a given message with every chat parser added,
 * then add a copy of message to parsedMessages list
 *
 * @param {Object|Array} msg
 * @return {Object} msg
 */
function parseMessage (msg)
{
  if (
    ['GENERAL', 'TRADE', 'SYSTEM', 'COMMERCIAL'].includes(msg.channel) ||
    (!settings.recordGlobalChatChannel && msg.channel === 'GLOBAL')
  )
  {
    return
  }

  const parsedMsg = {
    channel: msg.channel,
    author: msg.nick ?? msg.authorBusinessCard?.getNick() ?? '',
    receiver: msg.receiverBusinessCard?.getNick(),
    command: getCommandFromMsg(msg),
    text: getTextFromMessage(msg),
    ts: msg.ts
  }

  for (const parser of chatParserFunctionList) parser(parsedMsg)
  parsedMessages.push(parsedMsg)
}

export function initChatParser ()
{
  if (window.narratorTools.interface === 'NI')
  {
    const oldAddMessage = window.Engine.chatController.addMessage
    window.Engine.chatController.addMessage = function (data)
    {
      console.log('ADD MESSAGE')
      parseMessage(data)
      return oldAddMessage.apply(window.Engine.chatController, arguments)
    }
  }
  else
  {
    const oldAddMessage = window.g.chatController.addMessage
    window.g.chatController.addMessage = function (data)
    {
      parseMessage(data)
      return oldAddMessage.apply(window.g.chatController, arguments)
    }
  }
}
