export function loadFavourite (type, id)
{
  const obj = window.localStorage.getItem('narratorTools-' + type + '-' + id)
  if (obj)
  {
    return JSON.parse(obj)
  }
  else
  {
    return false
  }
}

export function saveFavourite (type, id, obj)
{
  window.localStorage.setItem('narratorTools-' + type + '-' + id, JSON.stringify(obj))
}

export function loadData (e)
{
  // const id = checkId(e)
  console.log('WIP')
}

export function updateSingleCell (cell, data)
{
  if (typeof data === 'object')
  {
    if (data.name)
    {
      cell.setAttribute('tip', data.name)
      cell.setAttribute('data-tip', data.name)
    }
    else
    {
      cell.removeAttribute('tip')
      cell.removeAttribute('data-tip')
    }
    cell.children[0].src = data.url
    return true
  }
  cell.children[0].src = 'https://micc.garmory-cdn.cloud/obrazki/npc/mas/nic32x32.gif'
  return false
}
