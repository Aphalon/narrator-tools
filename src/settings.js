export const settings = {
  coordSearch: false,
  style: 'vertical',
  position: {
    left: '',
    top: ''
  },
  visible: true,
  draggable: false,
  addGraf: {
    selectedTab: 0,
    selectedType: 'basic',
    customTabNames: [
      'Własne 1',
      'Własne 2',
      'Własne 3',
      'Własne 4',
      'Własne 5',
      'Własne 6',
      'Własne 7',
      'Własne 8',
      'Własne 9',
      'Własne 10'
    ],
    customNpcs: {}
  },
  attachNPCToMouse: true,
  lastVisited: 'addGraf',
  recording: false,
  recordingPrompt: true,
  recordGlobalChatChannel: false
}

const afterLoadSettingsCallbacks = []

export function addAfterLoadSettingsCallback (func)
{
  afterLoadSettingsCallbacks.push(func)
}

export function loadSettings ()
{
  const rawData = window.localStorage.getItem('narratorToolsSettings')
  if (rawData)
  {
    const data = JSON.parse(rawData)
    for (const option in settings)
    {
      if (typeof data[option] !== 'undefined')
      {
        settings[option] = data[option]
      }
    }
  }
  else
  {
    saveSettings()
  }
  for (const func of afterLoadSettingsCallbacks)
  {
    func()
  }
  console.log(settings)
}

export function saveSettings ()
{
  console.log(settings)
  window.localStorage.setItem('narratorToolsSettings', JSON.stringify(settings))
}
