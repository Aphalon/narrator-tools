import { saveSettings, settings } from './settings'
import { setDraggable } from './dragging'
import { setTip } from './tips'

const CHAT_TAB = {
  GENERAL: 'GENERAL',
  GLOBAL: 'GLOBAL',
  LOCAL: 'LOCAL',
  TRADE: 'TRADE',
  GROUP: 'GROUP',
  CLAN: 'CLAN',
  SYSTEM: 'SYSTEM',
  PRIVATE: 'PRIVATE',
  COMMERCIAL: 'COMMERCIAL'
}
const popupParent = window.narratorTools.interface === 'SI' ? document.body : document.querySelector('.mAlert-layer')
const sendSettings = {
  PRIV: true,
  GRP: true,
  CLAN: false,
  EMO: false
}
let messageList = {}
const msgsToDelete = []
let isCurrentlySendingData = false

const recordingSymbol = document.createElement('div')
recordingSymbol.className = 'recording'
setTip(recordingSymbol, 'Kliknij aby zacząć nagrywanie')
recordingSymbol.addEventListener('click', startRecording)

function convertStringToAscii (string)
{
  return string.trim().replace(/[^a-zńąćśźżóęł_ 0-9]/gi, '')
    .replace(/ą/g, 'a')
    .replace(/Ą/g, 'A')
    .replace(/ę/g, 'e')
    .replace(/Ę/g, 'E')
    .replace(/ł/g, 'l')
    .replace(/Ł/g, 'L')
    .replace(/[żź]/g, 'z')
    .replace(/[ŻŹ]/g, 'Z')
    .replace(/ó/g, 'o')
    .replace(/Ó/g, 'O')
    .replace(/ń/g, 'n')
    .replace(/Ń/g, 'N')
    .replace(/ć/g, 'c')
    .replace(/Ć/g, 'C')
    .replace(/ś/g, 's')
    .replace(/Ś/g, 'S')
    .replace(/ /g, '_')
}

function sendDataToServer (sessionName, fileObj)
{
  if (isCurrentlySendingData) return
  isCurrentlySendingData = true

  const ansiSessionName = convertStringToAscii(sessionName)
  const fileName = `${getDateString()}_${ansiSessionName}_${
    convertStringToAscii(window.narratorTools.sharedFunctions.getHeroNick())
  }`

  const formattedFileString = JSON.stringify(fileObj)
  const desc = fileObj[0][2]
  console.log(formattedFileString.length)

  const sendingWindow = document.createElement('div')
  sendingWindow.id = 'log-saver__sending-window'
  sendingWindow.className = 'log-saver__popup'

  sendingWindow.appendChild(createHeader('Kopiowanie do repozytorium', sendingWindow))

  const background = document.createElement('div')
  background.className = 'log-saver__background'
  sendingWindow.appendChild(background)

  const textToRepoContainer = document.createElement('div')
  textToRepoContainer.className = 'log-saver__text-to-repo-container'
  const textToRepository = document.createElement('span')
  textToRepository.className = 'log-saver__text-to-repo'
  textToRepository.innerText =
    `### ${getDateString()}, ${sessionName}, ${window.narratorTools.sharedFunctions.getHeroNick()}
*Lokalizacje: *
Logi: https://krisaphalon.ct8.pl/get/${fileName}

${desc}
`
  setTip(textToRepository, 'Kliknij, aby skopiować')
  textToRepoContainer.addEventListener('click', () =>
  {
    navigator.clipboard.writeText(textToRepository.innerText).then(() => window.message('Skopiowano!'))
  })
  textToRepoContainer.appendChild(textToRepository)
  background.appendChild(textToRepoContainer)

  const loadingContainer = document.createElement('div')
  loadingContainer.className = 'log-saver__loading-container'
  const loadingBarContainer = document.createElement('div')
  loadingBarContainer.className = 'log-saver__loading-bar-container'
  const loadingBarFill = document.createElement('div')
  loadingBarFill.style.width = '0'
  loadingBarFill.className = 'log-saver__loading-bar-fill'
  loadingBarContainer.appendChild(loadingBarFill)
  const loadingStatus = document.createElement('span')
  loadingStatus.className = 'log-saver__loading-status'
  loadingBarContainer.appendChild(loadingStatus)

  loadingContainer.appendChild(loadingBarContainer)
  background.appendChild(loadingContainer)
  setDraggable(sendingWindow)
  popupParent.appendChild(sendingWindow)

  const dataParts = formattedFileString.match(/.{1,5000}/g)
  const dataPartsStartLength = dataParts.length

  function sendPart ()
  {
    const percentDone = (((dataPartsStartLength - dataParts.length) / dataPartsStartLength) * 100).toFixed() + '%'
    loadingBarFill.style.width = percentDone
    switch (percentDone)
    {
      case '0%':
        loadingStatus.innerText = 'Inicjowanie...'
        break
      case '100%':
        loadingStatus.innerText = 'Parsowanie na serwerze...'
        endRecording()
        fetch('https://krisaphalon.ct8.pl/rebuild-panel', { method: 'POST' }).then(() =>
        {
          loadingStatus.innerText = 'Zapisano!'
          isCurrentlySendingData = false
        })
        break
      default:
        loadingStatus.innerText = percentDone
    }

    if (dataParts.length > 0)
    {
      const xhr = new window.XMLHttpRequest()
      xhr.onreadystatechange = function ()
      {
        if (this.readyState === 4 && this.status === 200)
        {
          dataParts.shift()
          sendPart()
        }
      }
      // http://localhost:3000/upload
      // http://krisaphalon.ct8.pl/upload
      xhr.open('POST', 'https://krisaphalon.ct8.pl/upload', true)
      xhr.setRequestHeader('File-Name', fileName)
      xhr.send(dataParts[0])
    }
  }

  sendPart()
}

function createHeader (headerName, windowToClose)
{
  const fragment = document.createDocumentFragment()

  const header = document.createElement('div')
  header.className = 'header-label-positioner'

  const headerLabel = document.createElement('div')
  headerLabel.className = 'header-label'

  const headerDecorLeft = document.createElement('div')
  headerDecorLeft.className = 'left-decor'
  headerLabel.appendChild(headerDecorLeft)
  const headerDecorRight = document.createElement('div')
  headerDecorRight.className = 'right-decor'
  headerLabel.appendChild(headerDecorRight)
  const headerText = document.createElement('span')
  headerText.innerText = headerName
  headerLabel.appendChild(headerText)

  header.appendChild(headerLabel)

  fragment.appendChild(header)

  const closeDecor = document.createElement('div')
  closeDecor.className = 'log-saver__close-decor'
  const closeButton = document.createElement('button')
  closeButton.className = 'log-saver__close-button'
  closeButton.addEventListener('click', () =>
  {
    windowToClose.parentNode.removeChild(windowToClose)
  })
  closeDecor.appendChild(closeButton)
  fragment.appendChild(closeDecor)
  return fragment
}

function createModeratePriv ()
{
  const elm = document.createElement('div')
  elm.className = 'moderate-priv'

  const background = document.createElement('div')
  background.className = 'log-saver__background show-priv'

  function checkModeration ()
  {
    msgsToDelete.splice(0, msgsToDelete.length)
    background.childNodes.forEach(function (childNode)
    {
      if (!childNode.children[0].checked) msgsToDelete.push(childNode.getAttribute('data-id'))
    })
  }

  const checkAll = document.createElement('button')
  checkAll.className = 'log-saver__button'
  checkAll.innerText = '✓'
  setTip(checkAll, 'Zaznacz wszystkie wiadomości, sprawiając, że wszystkie zostaną przesłane')
  checkAll.addEventListener('click', function ()
  {
    background.childNodes.forEach(function (childNode)
    {
      if (
        (background.classList.contains('show-priv') && childNode.classList.contains('priv')) ||
        (background.classList.contains('show-grp') && childNode.classList.contains('grp'))
      )
      {
        childNode.children[0].checked = true
      }
    })
    checkModeration()
  })
  elm.appendChild(checkAll)

  const uncheckAll = document.createElement('button')
  uncheckAll.className = 'log-saver__cancel-button log-saver__button'
  uncheckAll.innerText = 'x'
  setTip(uncheckAll, 'Odznacz wszystkie wiadomości, sprawiając, że żadna z nich nie zostanie przesłana')
  uncheckAll.addEventListener('click', function ()
  {
    background.childNodes.forEach(function (childNode)
    {
      if (
        (background.classList.contains('show-priv') && childNode.classList.contains('priv')) ||
        (background.classList.contains('show-grp') && childNode.classList.contains('grp'))
      )
      {
        childNode.children[0].checked = false
      }
    })
    checkModeration()
  })
  elm.appendChild(uncheckAll)

  for (const ts in messageList)
  {
    if (messageList[ts].command.includes('priv') || messageList[ts].command.includes('grp'))
    {
      const messageElm = document.createElement('li')
      if (messageList[ts].command.includes('priv')) messageElm.className = 'priv'
      else messageElm.className = 'grp'

      messageElm.setAttribute('data-id', ts)

      setTip(messageElm, '«' + messageList[ts].nick + '» ' + messageList[ts].content)
      const input = document.createElement('input')
      input.type = 'checkbox'
      input.checked = !msgsToDelete.includes(ts)
      input.addEventListener('input', function ()
      {
        if (input.checked)
        {
          if (msgsToDelete.includes(ts)) msgsToDelete.splice(msgsToDelete.indexOf(ts), 1)
        }
        else
        {
          msgsToDelete.push(ts)
        }
      })
      messageElm.appendChild(input)
      const span = document.createElement('span')
      span.innerText = messageList[ts].content
      messageElm.appendChild(span)
      background.appendChild(messageElm)
    }
  }
  elm.appendChild(background)

  const toggleLabel = document.createElement('label')
  toggleLabel.className = 'switch'
  setTip(toggleLabel, 'Przełącz pomiędzy wiadomościami z czatu grupowego oraz prywatnego')
  const toggle = document.createElement('input')
  toggle.type = 'checkbox'
  toggle.addEventListener('change', function ()
  {
    if (toggle.checked)
    {
      background.classList.add('show-grp')
      background.classList.remove('show-priv')
    }
    else
    {
      background.classList.add('show-priv')
      background.classList.remove('show-grp')
    }
  })
  const slider = document.createElement('span')
  slider.className = 'round-slider'
  toggleLabel.appendChild(toggle)
  toggleLabel.appendChild(slider)
  elm.appendChild(toggleLabel)

  return elm
}

function isCommandBlacklisted (command)
{
  return (command.startsWith('priv') && !sendSettings.PRIV) ||
    (command.startsWith('grp') && !sendSettings.GRP) ||
    (command.startsWith('clan') && !sendSettings.CLAN) ||
    (command.startsWith('emo') && !sendSettings.EMO)
}

function sortAndFilterListToArray (list)
{
  const arr = []
  const keys = Object.keys(list)
  keys.sort()
  for (const key of keys)
  {
    if (!isCommandBlacklisted(list[key].command) && !msgsToDelete.includes(key))
    {
      arr.push(list[key])
    }
  }
  return arr
}

function saveData ()
{
  const a = window.document.createElement('a')
  a.href = window.URL.createObjectURL(new window.Blob(
    [JSON.stringify(messageList, null, 4)],
    { type: 'text/json' })
  )
  a.download = 'json.json'
  document.body.appendChild(a)
  a.click()
  document.body.removeChild(a)
}

function deleteData ()
{
  messageList = {}
  window.localStorage.removeItem('nerthusMessageList')
}

function promptDeleteData () // TODO change this window to prettier one
{
  if (window.narratorTools.interface === 'NI')
  {
    window.mAlert('Czy na pewno chcesz usunąć logi bez ich wysyłania?',
      [
        {
          txt: 'Tak',
          callback ()
          {
            endRecording()
            deleteData()
            return true
          }
        },
        {
          txt: 'Nie',
          callback ()
          {
            return true
          }
        }

      ])
  }
  else
  {
    window.mAlert('Czy na pewno chcesz usunąć logi bez ich wysyłania?', 2, [() =>
    {
      endRecording()
      deleteData()
    }, () => false])
  }
}

/**
 * @returns Object - {commands_count, nick: number_of_commands}
 */
function countPlayersInvolvement ()
{
  const playerList = {}
  let commandsCount = 0
  for (const messageTs in messageList)
  {
    const message = messageList[messageTs]
    if (message.nick !== '' && !isCommandBlacklisted(message.command))
    {
      console.log(message)
      const nick = message.nick.split(' -> ')[0]
      if (!playerList[nick]) playerList[nick] = 1
      else playerList[nick]++

      commandsCount++
    }
  }
  return { commandsCount, playerList }
}

function createInvolvedPlayer (name, involved)
{
  const player = document.createElement('li')
  player.className = 'log-saver__player-involved'
  const involvedCheckbox = document.createElement('input')
  involvedCheckbox.className = 'log-saver__player-involved-checkbox'
  involvedCheckbox.type = 'checkbox'
  involvedCheckbox.checked = involved
  player.appendChild(involvedCheckbox)
  const nick = document.createElement('span')
  nick.className = 'log-saver__player-involved-nick'
  nick.innerText = name
  player.appendChild(nick)
  return player
}

function createInvolvedPlayersList ()
{
  const fragment = document.createDocumentFragment()
  const { commandsCount, playerList } = countPlayersInvolvement()

  for (const playerName in playerList)
  {
    const overThreshold = playerList[playerName] > 25 ||
      (playerList[playerName] / commandsCount) > 0.2

    fragment.appendChild(createInvolvedPlayer(playerName, overThreshold))
  }

  return fragment
}

function getDateString ()
{
  const date = new Date()
  const year = date.getFullYear()
  const month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)
  const day = date.getDate() > 9 ? date.getDate() : '0' + date.getDate()
  return `${year}-${month}-${day}`
}

function showRecordUploadWindow ()
{
  const recordUploadWindow = document.createElement('div')
  recordUploadWindow.id = 'log-saver-popup'
  recordUploadWindow.className = 'log-saver__popup'

  const moderatePriv = createModeratePriv()

  recordUploadWindow.appendChild(createHeader('Przesyłanie', recordUploadWindow))

  const popupBackground = document.createElement('div')
  popupBackground.className = 'log-saver__background'

  const editorContainer = document.createElement('div')
  editorContainer.className = 'log-saver__editor-container'

  const textEditPanel = document.createElement('div')
  textEditPanel.className = 'log-saver__text-edit-panel'
  editorContainer.appendChild(textEditPanel)

  const titleEdit = document.createElement('input')
  titleEdit.className = 'log-saver__title-edit'
  setTip(titleEdit, 'Tytuł sesji')

  titleEdit.value = 'sesja fabularna'
  textEditPanel.appendChild(titleEdit)

  const synopsis = document.createElement('textarea')
  synopsis.id = 'log-saver__synopsis'
  synopsis.className = 'log-saver__synopsis'

  const synopsisLabel = document.createElement('label')
  synopsisLabel.htmlFor = synopsis.id
  synopsisLabel.className = 'log-saver__synopsis-label'
  synopsisLabel.innerText = 'Streszczenie (maksymalnie 100 słów)'
  textEditPanel.appendChild(synopsisLabel)
  textEditPanel.appendChild(synopsis)

  const optionsPanel = document.createElement('div')
  optionsPanel.className = 'log-saver__options-panel'
  editorContainer.appendChild(optionsPanel)

  const sessionType = document.createElement('div')
  sessionType.className = 'log-saver__session-type'

  const sessionSuccessfulContainer = document.createElement('div')
  sessionSuccessfulContainer.className = 'session-type-options-container'
  const sessionSuccessfulYes = document.createElement('input')
  sessionSuccessfulYes.className = 'log-saver__radio-input'
  sessionSuccessfulYes.id = 'session-successful-yes'
  sessionSuccessfulYes.type = 'radio'
  sessionSuccessfulYes.name = 'session-successful'
  sessionSuccessfulYes.value = 'yes'
  sessionSuccessfulContainer.appendChild(sessionSuccessfulYes)
  const labelSuccessfulYes = document.createElement('label')
  labelSuccessfulYes.className = 'log-saver__radio-label'
  labelSuccessfulYes.htmlFor = sessionSuccessfulYes.id
  labelSuccessfulYes.innerText = 'Udana'
  setTip(labelSuccessfulYes, 'Gracze dokonali znaczące kroki w celu zakończenia wątku lub też go zakończyli (z pozytywnym skutkiem)')
  sessionSuccessfulContainer.appendChild(labelSuccessfulYes)
  const sessionSuccessfulNo = document.createElement('input')
  sessionSuccessfulNo.className = 'log-saver__radio-input'
  sessionSuccessfulNo.id = 'session-successful-no'
  sessionSuccessfulNo.type = 'radio'
  sessionSuccessfulNo.name = 'session-successful'
  sessionSuccessfulNo.value = 'no'
  sessionSuccessfulContainer.appendChild(sessionSuccessfulNo)
  const labelSuccessfulNo = document.createElement('label')
  labelSuccessfulNo.className = 'log-saver__radio-label'
  labelSuccessfulNo.htmlFor = sessionSuccessfulNo.id
  labelSuccessfulNo.innerText = 'Nieudana'
  setTip(labelSuccessfulNo, 'Gracze nie dokonali znaczących kroków w celu zakończenia wątku lub też go zakończyli ze skutkiem negatywnym')
  sessionSuccessfulContainer.appendChild(labelSuccessfulNo)

  sessionType.appendChild(sessionSuccessfulContainer)

  const sessionSizeContainer = document.createElement('div')
  sessionSizeContainer.className = 'session-type-options-container'
  const sessionSizeLarge = document.createElement('input')
  sessionSizeLarge.className = 'log-saver__radio-input'
  sessionSizeLarge.id = 'session-size-large'
  sessionSizeLarge.type = 'radio'
  sessionSizeLarge.name = 'session-size'
  sessionSizeLarge.value = 'large'
  sessionSizeContainer.appendChild(sessionSizeLarge)
  const labelSizeLarge = document.createElement('label')
  labelSizeLarge.className = 'log-saver__radio-label'
  labelSizeLarge.htmlFor = sessionSizeLarge.id
  labelSizeLarge.innerText = 'Większa'
  setTip(labelSizeLarge, 'Sesja była obszerna w działania postaci i trwała więcej niż około dwóch godzin.')
  sessionSizeContainer.appendChild(labelSizeLarge)
  const sessionSizeSmall = document.createElement('input')
  sessionSizeSmall.className = 'log-saver__radio-input'
  sessionSizeSmall.id = 'session-size-small'
  sessionSizeSmall.type = 'radio'
  sessionSizeSmall.name = 'session-size'
  sessionSizeSmall.value = 'small'
  sessionSizeContainer.appendChild(sessionSizeSmall)
  const labelSizeSmall = document.createElement('label')
  labelSizeSmall.className = 'log-saver__radio-label'
  labelSizeSmall.htmlFor = sessionSizeSmall.id
  labelSizeSmall.innerText = 'Mniejsza'
  setTip(labelSizeSmall, 'Działania postaci były dosyć skąpe a sama sesja trwała mniej niż około dwie godziny')
  sessionSizeContainer.appendChild(labelSizeSmall)
  sessionType.appendChild(sessionSizeContainer)

  optionsPanel.appendChild(sessionType)

  const playersInvolved = document.createElement('ul')
  playersInvolved.className = 'log-saver__players-involved'
  setTip(playersInvolved, 'Gracze, którzy wzięli udział w sesji. (Zaznaczone nicki)')
  playersInvolved.appendChild(createInvolvedPlayersList())
  optionsPanel.appendChild(playersInvolved)

  popupBackground.appendChild(editorContainer)

  const buttonContainer = document.createElement('div')
  buttonContainer.className = 'log-saver__buttons-container'

  const togglePrivEdit = document.createElement('button')
  togglePrivEdit.className = 'log-saver__button log-saver__moderate-priv-button'
  setTip(togglePrivEdit, 'Szybka edycja')
  togglePrivEdit.addEventListener('click', () =>
  {
    moderatePriv.classList.toggle('expanded')
  })
  buttonContainer.appendChild(togglePrivEdit)

  const saveButton = document.createElement('button')
  saveButton.className = 'log-saver__save-button log-saver__button'
  saveButton.innerText = 'Zapisz'
  setTip(saveButton, 'Zatwierdź ustawienia, wyślij dane na serwer i wyświetl tekst do wklejenia do repozytorium.')
  saveButton.addEventListener('click', () =>
  {
    let success
    if (sessionSuccessfulYes.checked) success = true
    else if (sessionSuccessfulNo.checked) success = false
    else success = undefined

    let size
    if (sessionSizeLarge.checked) size = 'large'
    else if (sessionSizeSmall.checked) size = 'small'
    else size = undefined

    const comment = synopsis.value

    const playerList = []
    for (const elm of playersInvolved.childNodes)
    {
      if (elm.children[0].checked) playerList.push(elm.children[1].innerText)
    }

    sendDataToServer(titleEdit.value, [[success, size, comment, playerList], sortAndFilterListToArray(messageList)])
  })
  buttonContainer.appendChild(saveButton)

  const downloadButton = document.createElement('button')
  downloadButton.className = 'log-saver__download-button log-saver__button'
  downloadButton.innerText = 'Pobierz'
  downloadButton.addEventListener('click', saveData)
  setTip(downloadButton, 'Pobierz plik .json z nieprzetworzonymi logami na swój komputer')
  buttonContainer.appendChild(downloadButton)

  const cancelButton = document.createElement('button')
  cancelButton.className = 'log-saver__cancel-button log-saver__button'
  cancelButton.innerText = 'Usuń'
  cancelButton.addEventListener('click', promptDeleteData)
  setTip(cancelButton, 'Zakończ nagrywanie bez wysyłania logów na serwer. Uwaga to działanie usunie logi zapisane lokalnie.')
  buttonContainer.appendChild(cancelButton)

  popupBackground.appendChild(buttonContainer)

  recordUploadWindow.appendChild(popupBackground)
  recordUploadWindow.appendChild(moderatePriv)
  setDraggable(recordUploadWindow)
  popupParent.appendChild(recordUploadWindow)
  titleEdit.focus()
}

function startRecording ()
{
  settings.recording = true
  saveSettings()
  window.localStorage.setItem('nerthus-recording', 'true')
  recordingSymbol.classList.add('on')
  setTip(recordingSymbol, 'Kliknij, aby wyświetlić panel wysyłania logów')
  recordingSymbol.addEventListener('click', showRecordUploadWindow)
  recordingSymbol.removeEventListener('click', startRecording)
  return true
}

function endRecording ()
{
  const logSaverPopup = document.getElementById('log-saver-popup')
  if (logSaverPopup)
  {
    logSaverPopup.style.opacity = '0'
    logSaverPopup.style.pointerEvents = 'none'
    setTimeout(() =>
    {
      popupParent.removeChild(logSaverPopup)
    }, 2000)
  }
  settings.recording = false
  saveSettings()
  recordingSymbol.classList.remove('on')
  setTip(recordingSymbol, 'Kliknij aby zacząć nagrywanie')
  recordingSymbol.removeEventListener('click', showRecordUploadWindow)
  recordingSymbol.addEventListener('click', startRecording)
  deleteData()
}

function startRecordingPopup ()
{
  if (window.narratorTools.interface === 'SI')
  {
    window.mAlert('Chcesz zacząć nagrywanie?',
      2, [function ()
      {
        startRecording()
      }, function ()
      {
        window.sessionStorage.setItem('log-saver', 'no-recording')
        return false
      }])
  }
  else
  {
    window.mAlert('Chcesz zacząć nagrywanie?', [
      {
        txt: 'Tak',
        callback: startRecording
      },
      {
        txt: 'Nie',
        callback ()
        {
          window.sessionStorage.setItem('log-saver', 'no-recording')
          return true
        }
      }
    ])
  }
}

function isCommand (msg)
{
  const arr = /(nar[01236]?|me|sys|dial666|dial[123]?|addGraf|delGraf|hide|light|map|resetMap|weather)/.exec(msg.s)
  if (arr) return arr[1]
}

function saveMessage (msg)
{
  const { author, text, command, channel, receiver, ts } = msg
  let commandClass = command
  let nick = author
  switch (channel)
  {
    case CHAT_TAB.GLOBAL:
      commandClass = ('global ' + command).trim()
      break
    case CHAT_TAB.CLAN:
      commandClass = ('clan ' + command).trim()
      break
    case CHAT_TAB.GROUP:
      commandClass = ('grp ' + command).trim()
      break
    case CHAT_TAB.PRIVATE:
      commandClass = ('priv ' + command).trim()
      nick = `${author} -> ${receiver}`
      break
  }

  const message = {
    nick,
    command: commandClass,
    content: text,
    date: ts
  }

  if (messageList[ts] && messageList[ts].nick === message.nick) return

  messageList[ts] = message
  window.localStorage.setItem('nerthusMessageList', JSON.stringify(messageList))
}

window.viewMessageList = function ()
{
  return messageList
}

export function initLogSaver ()
{
  if (window.narratorTools.interface === 'NI')
  {
    document.querySelector('.control-wrapper').appendChild(recordingSymbol)
  }
  else document.getElementById('chat').appendChild(recordingSymbol)

  if (settings.recording === true)
  {
    startRecording()
    messageList = window.localStorage.getItem('nerthusMessageList')
      ? JSON.parse(window.localStorage.getItem('nerthusMessageList'))
      : {}
  }

  window.narratorTools.sharedFunctions.addToChatParser(function (msg)
  {
    saveMessage(msg)
    if (
      !settings.recording &&
      settings.recordingPrompt &&
      isCommand(msg) &&
      !window.sessionStorage.getItem('log-saver')
    )
    {
      startRecordingPopup()
    }
  })
}
