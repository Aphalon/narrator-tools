window.narratorTools.graphicsToDisplay = []
window.narratorTools.redSquare = []
window.narratorTools.npcUnderlay = []
window.narratorTools.npcUnderlayColor = 'white'

/*
 *  NI would crash script if you try to remove callback and there are none,
 *  but there is no way of checking if such callback exists.
 *  Therefor this variable is needed.
 */
let underlayCallbackAdded = false

const tmpEmotionsDraw = window.Engine.emotions.getDrawableList
window.Engine.emotions.getDrawableList = function ()
{
  const ret = tmpEmotionsDraw()
  ret.push({
    draw (canvasRenderingContext)
    {
      if (window.narratorTools.redSquare.length > 0)
      {
        const style = canvasRenderingContext.fillStyle
        canvasRenderingContext.fillStyle = '#F00'
        canvasRenderingContext.globalAlpha = 0.7
        canvasRenderingContext.fillRect(
          window.narratorTools.redSquare[0] - window.Engine.map.offset[0],
          window.narratorTools.redSquare[1] - window.Engine.map.offset[1],
          32,
          32
        )
        canvasRenderingContext.globalAlpha = 1.0
        canvasRenderingContext.fillStyle = style
      }

      const style = canvasRenderingContext.fillStyle
      canvasRenderingContext.fillStyle = window.narratorTools.npcUnderlayColor
      const len = window.narratorTools.npcUnderlay.length
      for (let i = 0; i < len; i++)
      {
        canvasRenderingContext.fillRect(
          window.narratorTools.npcUnderlay[i][0] - window.Engine.map.offset[0],
          window.narratorTools.npcUnderlay[i][1] - window.Engine.map.offset[1],
          window.narratorTools.npcUnderlay[i][2],
          window.narratorTools.npcUnderlay[i][3]
        )
      }
      canvasRenderingContext.fillStyle = style
    },
    getOrder ()
    {
      return 0
    }
  })
  return ret
}

export function paintSquare (x, y)
{
  const _x = 32 * x
  const _y = 32 * y
  window.narratorTools.redSquare = [_x, _y]
}

export function removeSquare ()
{
  window.narratorTools.redSquare = []
}

/**
 * @param e {MouseEvent}
 */
export function getObjectUnderMouse (e) // BUG when clicking on yourself it returns undefined
{
  const obj = window.Engine.eventCollider.topHoverObject
  if (obj.d && obj.canvasObjectType !== 'MAP')
  {
    if (obj.d.nick === undefined || obj.d.clan !== undefined)
    {
      return false
    }
    const addOnStart = window.CFG.a_npath
    return {
      id: obj.d.id,
      nick: obj.d.nick.startsWith('<b>') ? obj.d.nick.replace(/<\/?b>/g, '') : obj.d.nick,
      icon: (obj.d.icon.startsWith('/obrazki') || obj.d.icon.startsWith('http'))
        ? obj.d.icon
        : addOnStart + obj.d.icon,
      x: obj.d.x,
      y: obj.d.y,
      collision: window.Engine.map.col.check(obj.d.x, obj.d.y) > 0
    }
  }
  else // try to check if it's type is 4
  {
    const canvasBounding = document.getElementById('GAME_CANVAS').getBoundingClientRect()
    const x = e.clientX - canvasBounding.left + window.scrollX / window.Engine.zoomFactor + window.Engine.map.offset[0]
    const y = e.clientY - canvasBounding.top + window.scrollY / window.Engine.zoomFactor + window.Engine.map.offset[1]

    const npcs = window.Engine.npcs.getDrawableList()
    for (const npc of npcs)
    {
      if (!npc.d)
      {
        continue
      }
      if (npc.d.type === 4)
      {
        const collider =
          [
            npc.rx * 32 + 16 - npc.fw / 2,
            npc.ry * 32 + 32 - npc.fh,
            npc.rx * 32 + 16 + npc.fw / 2,
            npc.ry * 32 + 32
          ]
        if (x >= collider[0] && x <= collider[2] && y >= collider[1] && y <= collider[3])
        {
          let addOnStart = ''
          if (!/\/obrazki\/npc\//.exec(obj.d.icon))
          {
            addOnStart = window.CFG.a_npath
          }

          return {
            id: npc.d.id,
            nick: '',
            icon: addOnStart + npc.d.icon,
            x: npc.d.x,
            y: npc.d.y,
            collision: window.Engine.map.col.check(npc.d.x, npc.d.y) > 0
          }
        }
      }
    }
  }
  return false
}

/*
 * It's not the most efficient method, but it solves problem with npcs that are hidden by global addon
 */
function calculateNpcUnderlay ()
{
  window.narratorTools.npcUnderlay = []
  const npcs = window.Engine.npcs.getDrawableList()
  for (const npc of npcs)
  {
    window.narratorTools.npcUnderlay.push(
      [
        npc.rx * 32 + 16 - npc.fw / 2,
        npc.ry * 32 + 32 - npc.fh,
        npc.fw,
        npc.fh
      ]
    )
  }
}

function handleNewNpc (npcData)
{
  const oldAfterFetch = npcData.afterFetch
  npcData.afterFetch = function (f, v, d)
  {
    oldAfterFetch(f, v, d)
    calculateNpcUnderlay()
  }
  calculateNpcUnderlay()
}

export function underlayNPCs (color)
{
  window.narratorTools.npcUnderlayColor = color
  if (!underlayCallbackAdded)
  {
    window.API.addCallbackToEvent('newNpc', handleNewNpc)
    window.API.addCallbackToEvent('removeNpc', calculateNpcUnderlay)
    underlayCallbackAdded = true
  }
}

export function endUnderlayNPCs ()
{
  window.narratorTools.npcUnderlay = []
  if (underlayCallbackAdded)
  {
    window.API.removeCallbackFromEvent('newNpc', handleNewNpc)
    window.API.removeCallbackFromEvent('removeNpc', calculateNpcUnderlay)
  }
  underlayCallbackAdded = false
}
