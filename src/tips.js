export function setTip (object, tip, bold = false)
{
  switch (window.narratorTools.interface)
  {
    case 'SI':
      if (bold) object.setAttribute('tip', `<b>${tip}</b>`)
      else object.setAttribute('tip', tip)
      break
    case 'NI':
      if (bold) $(object).tip(`<strong>${tip}</strong>`)
      else $(object).tip(tip)
      break
  }
}

export function removeTip (object)
{
  if (window.narratorTools.interface === 'SI') object.removeAttribute('tip')
  else $(object).tip('')
}

export function setTipType (object, tipType)
{
  if (window.narratorTools.interface === 'SI') object.setAttribute('ctip', tipType)
}
