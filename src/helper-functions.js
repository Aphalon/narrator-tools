/**
 * @module helper_functions
 */

/**
 * Loads JSON from given url and executes it's result on a function
 * @function
 * @param {string} url
 * @param {function} callback
 */

export function loadJSON (url, callback)
{
  const xobj = new window.XMLHttpRequest()
  xobj.overrideMimeType('application/json')
  xobj.open('GET', url, true)
  xobj.onreadystatechange = function ()
  {
    if (xobj.readyState === 4 && xobj.status === 200)
    {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      callback(xobj.responseText)
    }
  }
  xobj.send(null)
}

export function importAll (r, importTo)
{
  r.keys().forEach(function (key)
  {
    importTo.push(r(key))
  })
}

/**
 * Checks id of clicked item that has it's id after 3rd '-'
 * @function
 * @param {MouseEvent} e
 * @returns {number | boolean} id number of false when invalid
 */
export function checkId (e)
{
  if (e.target !== e.currentTarget)
  {
    const clickedItem = e.target.id
    return parseInt(clickedItem.split('-')[3])
  }
  else
  {
    return false
  }
}

export function afterNerthusDefined (callback)
{
  if (window.nerthus)
  {
    if (INTERFACE === 'NI' && !document.getElementsByClassName('widget-nerthus')[0])
    {
      setTimeout(afterNerthusDefined.bind(this, callback), 50)
    }
    else
    {
      if (window.nerthus.loaded) callback()
      else window.nerthus.addCallbackToEvent('loaded', callback)
    }
  }
  else
  {
    setTimeout(afterNerthusDefined.bind(this, callback), 50)
  }
}
