import { importAll } from './helper-functions'
import { deleteNpcContainer, initDeleteBox } from './npc-remover'
import { saveSettings, settings } from './settings'

export let currentlyDrawnModule = ''

export const frameElement = document.createElement('div')
export const mainModuleSpace = document.createElement('div')

export const elements = {
  nav: document.createElement('nav'),
  body: document.createElement('div')
}

const cache = []
importAll(require.context('./modules/', true, /\.js$/), cache)
const modules = {}
for (const mod of cache)
{
  if (typeof mod.default !== 'undefined' &&
    typeof mod.default.init === 'function' &&
    typeof mod.default.draw === 'function')
  {
    const button = mod.default.init()
    button.addEventListener('click', function ()
    {
      settings.lastVisited = mod.default.name
      saveSettings()
    })
    modules[mod.default.name] = {
      orderId: mod.default.orderId,
      button: button,
      drawFunction: mod.default.draw
    }
  }
}
const sortedModulesNames = Object.keys(modules).sort(function (a, b)
{
  return modules[a].orderId - modules[b].orderId
})
const moduleCount = sortedModulesNames.length
for (let i = 0; i < moduleCount; i++)
{
  elements.nav.appendChild(modules[sortedModulesNames[i]].button)
}

export function toggleMainFrameVisibility (e)
{
  e.preventDefault()
  if (frameElement.parentElement === null)
  {
    drawMainFrame()
    displayModule(settings.lastVisited)
    settings.visible = true
  }
  else if (frameElement.style.visibility === 'visible')
  {
    frameElement.style.visibility = 'hidden'
    settings.visible = false
  }
  else
  {
    frameElement.style.visibility = 'visible'
    settings.visible = true
  }
  saveSettings()
  return settings.visible
}

export function drawMainFrame ()
{
  // TODO clean up ids
  initDeleteBox()
  const canvas = document.createDocumentFragment()
  frameElement.id = 'narrator-tools'
  /*
   * We overwrite the display in stylesheet so that addon only shows when one of stylesheets is loaded.
   * This fixes strange graphical bugs when switching from one to another stylesheet
   * or in brief second after entering the game.
   */
  frameElement.style.display = 'none'
  frameElement.style.visibility = 'visible'

  if (narratorTools.interface === 'NI')
  {
    frameElement.style.position = 'absolute'
  }

  elements.body.className = 'NT-background'

  mainModuleSpace.id = 'main-control-box'
  mainModuleSpace.appendChild(elements.nav)

  const deleteBox = document.createElement('div')
  deleteBox.id = 'delete-NPC-box'
  deleteNpcContainer.id = 'npc-container'
  deleteBox.appendChild(deleteNpcContainer)

  elements.body.appendChild(mainModuleSpace)
  elements.body.appendChild(deleteBox)
  frameElement.appendChild(elements.body)
  canvas.appendChild(frameElement)
  if (INTERFACE === 'NI')
  {
    if (settings.style === 'vertical')
    {
      document.querySelector('.right-column.main-column').after(canvas)
    }
    else
    {
      document.getElementsByClassName('bottom positioner')[0].appendChild(canvas)
    }
  }
  else
  {
    document.getElementById('centerbox2').appendChild(canvas)
  }
}

/**
 * Displays module in mainModuleSpace
 *
 * @param {string} name - Name of module to display
 * @returns {boolean} whether module with such name exists or not
 */
export function displayModule (name)
{
  if (currentlyDrawnModule !== name && modules[name] !== undefined)
  {
    let drawnElement
    const len = mainModuleSpace.children.length
    for (let i = 0; i < len; i++)
    {
      if (mainModuleSpace.children[i].tagName.toLowerCase() === 'div')
      {
        if (mainModuleSpace.children[i].id === name + '-tab')
        {
          drawnElement = mainModuleSpace.children[i]
        }
        else
        {
          mainModuleSpace.children[i].style.display = 'none'
        }
      }
    }

    if (drawnElement)
    {
      drawnElement.style.display = ''
    }
    else
    {
      modules[name].drawFunction()
    }

    currentlyDrawnModule = name
    return true
  }
  return false
}
