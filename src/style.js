import { saveSettings, settings } from './settings'

const element = document.createElement('link')
element.rel = 'stylesheet'
document.head.appendChild(element)

const CDN_URL = 'https://glcdn.githack.com/Aphalon/narrator-tools/raw/master/'
const STYLE_VERSION = '407'
const styleUrl = {
  horizontal: `${CDN_URL}styles/horizontal.css?version=${STYLE_VERSION}`,
  vertical: `${CDN_URL}styles/vertical.css?version=${STYLE_VERSION}`
}

export function loadStyle (style)
{
  switch (style) // 0: Horizontal, 1: Vertical
  {
    case 1:
    case 'vertical':
      settings.style = 'vertical'
      element.href = styleUrl.vertical
      break
    case 0:
    case 'horizontal':
    default:
      settings.style = 'horizontal'
      element.href = styleUrl.horizontal
      break
  }
  saveSettings()
}
