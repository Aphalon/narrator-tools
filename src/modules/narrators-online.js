import { displayModule, mainModuleSpace } from '../main-panel'
import { setTip } from '../tips'

export default new function ()
{
  this.name = 'narrators'
  this.orderId = 50

  const frame = document.createElement('div')
  const narratorsFrame = document.createElement('div')

  const loader = document.createElement('div')

  function newNarrator (name, type)
  {
    const div = document.createElement('div')
    div.innerText = name
    if (type === 'radny')
    {
      div.style.color = 'red'
    }
    div.onclick = function ()
    {
      if (INTERFACE === 'NI')
      {
        window.Engine.chat.replyTo(name)
      }
      else
      {
        window.chatTo(name)
        document.getElementById('inpchat').focus()
      }
    }
    narratorsFrame.appendChild(div)
  }

  async function fetchOnlineList ()
  {
    while (narratorsFrame.firstChild)
    {
      narratorsFrame.removeChild(narratorsFrame.firstChild)
    }
    try
    {
      loader.classList.add('active')
      const data = await fetch('https://public-api.margonem.pl/info/online/nerthus.json')
      loader.classList.remove('active')
      for (const player of await data.json())
      {
        const permissionLvl = nerthus.permissions.checkPermissionLvl(player.n)
        if (permissionLvl === 2) newNarrator(player.n, 'radny')
        else if (permissionLvl === 1) newNarrator(player.n)
      }
    }
    catch (error)
    {
      loader.classList.remove('active')
      const errorMsg = document.createElement('div')
      errorMsg.innerText = 'Coś poszło nie tak...'
      errorMsg.style.color = 'red'
      narratorsFrame.appendChild(errorMsg)
    }
  }

  this.draw = function ()
  {
    frame.id = 'narrators-tab'
    frame.style.display = 'flex'

    const refresh = document.createElement('button')
    refresh.innerText = 'Refresh'
    refresh.className = 'button'
    refresh.addEventListener('click', fetchOnlineList, false)
    frame.appendChild(refresh)

    loader.className = 'loader'
    frame.appendChild(loader)

    narratorsFrame.id = 'narrators-box'
    frame.appendChild(narratorsFrame)

    mainModuleSpace.appendChild(frame)

    fetchOnlineList()
    setInterval(fetchOnlineList, 120000)
  }

  this.init = function ()
  {
    const navButton = document.createElement('button')
    navButton.id = 'open-narrators'
    setTip(navButton, 'Narratorzy Online')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    const navImage = document.createElement('img')
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/itemy/neu/nert-kart-bibl.gif'
    navButton.appendChild(navImage)

    return navButton
  }
}()
