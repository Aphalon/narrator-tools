import { checkId, loadJSON } from '../helper-functions'
import lastInput from '../last-input'
import { addNpc, hideNpc } from '../chat'
import { displayModule, mainModuleSpace } from '../main-panel'
import { saveSettings, settings } from '../settings'
import { attachNpcToMouse, updateAttachedNpcInformation } from '../lift-and-drop'
import { setTip, setTipType } from '../tips'

/**
 * Creates addGraf module
 *
 * @author: Kris Aphalon
 * @constructor
 */
export default new function ()
{
  /**
   * Name of whole module
   * @type {string}
   */
  this.name = 'addGraf'
  this.orderId = 10

  const ADD_GRAF_TYPES = {
    BASIC: 'basic',
    CUSTOM: 'custom'
  }
  const frame = document.createElement('div')
  const inputFrame = document.createElement('div')
  const favouriteFrame = document.createElement('div')
  const tabSelect = document.createElement('div')
  const npcSelect = document.createElement('div')
  const coordCheck = document.createElement('button')
  const getNpcFromMap = document.createElement('button')
  const hidePermanent = document.createElement('button')
  const confirmButton = document.createElement('button')

  const basicFavourites = document.createElement('button')
  const customFavourites = document.createElement('button')

  /**
   * Unified id that tab-change buttons use.
   * After last dash there should be id of a tab.
   * @type {string}
   */
  const tabSelectButtonId = 'NT-addGraf-tabSelect-'

  const input = {
    x: document.createElement('input'),
    y: document.createElement('input'),
    url: document.createElement('input'),
    name: document.createElement('input'),
    collision: document.createElement('input')
  }

  const defaultTabNames = [
    'Strażnicy',
    'Chłopi',
    'Mieszczanie',
    'Arystokraci',
    'Magowie',
    'Ciemne typy',
    'Potwory',
    'Zwierzęta',
    'Obiekty',
    'Różne'
  ]
  // TODO: różni strażnicy w miastach

  let basicNPCs = []

  function changeFavouritesTo (tabNumber)
  {
    settings.addGraf.selectedTab = tabNumber
    saveSettings()

    for (const button of tabSelect.querySelectorAll('button'))
    {
      button.classList.remove('active')
      if (button.id === tabSelectButtonId + tabNumber)
      {
        button.classList.add('active')
      }
    }

    if (settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC && basicNPCs.length === 0)
    {
      setTimeout(changeFavouritesTo.bind(this, tabNumber), 200)
      return
    }

    const npcList = settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC ? basicNPCs : settings.addGraf.customNpcs
    const buttonClass = settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC ? 'basicNPCbutton' : 'customNPCbutton'

    const len = npcSelect.children.length
    for (let i = 0; i < len; i++)
    {
      setTip(npcSelect.children[i], npcList?.[tabNumber]?.[i]?.name ?? '', true)
      npcSelect.children[i].className = `favourites-div ${buttonClass}`
      npcSelect.children[i].children[0].src = npcList?.[tabNumber]?.[i]?.url ?? ''
    }
  }

  function loadNpc (e)
  {
    const id = checkId(e)

    const npc = settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC
      ? basicNPCs[settings.addGraf.selectedTab][id]
      : (settings.addGraf.customNpcs?.[settings.addGraf.selectedTab]?.[id])

    input.url.value = npc?.url ?? ''
    input.name.value = npc?.name ?? ''
    input.collision.checked = npc?.collision ?? false
    attachNpcToMouse(
      input.url.value,
      input.name.value,
      input.collision.checked,
      confirmButton
    )
  }

  function saveNpc (e)
  {
    e.preventDefault()
    if (settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC)
    {
      window.mAlert('Nie możesz nadpisać podstawowych grafik!<br>By zapisać swoje własne, kliknij przycisk \'własne\'.')
      return
    }
    const id = checkId(e)
    const npcObj = {
      name: input.name.value,
      url: input.url.value,
      collision: input.collision.checked
    }
    if (input.x.value !== '' && input.y.value !== '')
    {
      npcObj.x = input.x.value
      npcObj.y = input.y.value
    }
    if (!settings.addGraf.customNpcs[settings.addGraf.selectedTab]) settings.addGraf.customNpcs[settings.addGraf.selectedTab] = {}
    settings.addGraf.customNpcs[settings.addGraf.selectedTab][id] = npcObj
    setTip(e.target, npcObj.name, true)
    e.target.querySelector('img').src = npcObj.url || 'https://micc.garmory-cdn.cloud/obrazki/npc/mas/nic32x32.gif'

    saveSettings()
  }

  let coordSearchActive = false

  function searchCoords (e)
  {
    if (e.target === getNpcFromMap || e.target === coordCheck || e.target === hidePermanent)
    {
      if (e.target === coordCheck && coordSearchActive)
      {
        e.stopPropagation()
      }
      else if ((e.target === getNpcFromMap && npcSearchType === 'get') || (e.target === hidePermanent && npcSearchType === 'hide'))
      {
        npcSearchType = ''
        e.stopPropagation()
      }
      clearSearch()
    }
    else if (coordSearchActive)
    {
      e.stopPropagation()
      narratorTools.sharedFunctions.addLock('coordSearch')
      coordCheck.classList.remove('active')// todo

      const position = narratorTools.sharedFunctions.getMousePosition(e)
      input.x.value = position.x
      input.y.value = position.y
      lastInput.save('addGraf', inputFrame)
      coordHelper.show(position.x, position.y)
      coordSearchActive = false
      return false
    }
  }

  let npcSearchActive = false
  let npcSearchType = ''

  function searchNpc (e)
  {
    if (e.target === getNpcFromMap || e.target === coordCheck || e.target === hidePermanent)
    {
      if ((e.target === getNpcFromMap && npcSearchType === 'get') || (e.target === hidePermanent && npcSearchType === 'hide'))
      {
        npcSearchType = ''
        e.stopPropagation()
      }
      clearSearch()
    }
    else if (npcSearchActive)
    {
      e.stopPropagation()
      e.stopImmediatePropagation()
      narratorTools.sharedFunctions.addLock('npcSearch')

      const npc = window.narratorTools.sharedFunctions.getObjectUnderMouse(e)
      console.log(npc)

      if (npc)
      {
        console.log(npc)
        if (npcSearchType === 'get')
        {
          input.x.value = npc.x
          input.y.value = npc.y
          input.url.value = npc.icon
          input.name.value = npc.nick
          input.collision.checked = npc.collision

          getNpcFromMap.classList.remove('active')
          npcSearchActive = false
          window.narratorTools.sharedFunctions.endUnderlayNPCs()
          coordHelper.show(npc.x, npc.y)
          npcSearchType = ''
          return false
        }
        else if (npcSearchType === 'hide') // todo customnpc just remove
        {
          if (npc.id)
          {
            hideNpc(npc.id)
          }
          hidePermanent.classList.remove('active')
          npcSearchActive = false
          window.narratorTools.sharedFunctions.endUnderlayNPCs()
          npcSearchType = ''
          return false
        }
      }
    }
  }

  function clearSearch (e)
  {
    if (npcSearchActive || coordSearchActive)
    {
      if (e)
      {
        e.preventDefault()
        e.stopPropagation()
        npcSearchType = ''
      }
      npcSearchActive = false
      coordSearchActive = false

      getNpcFromMap.classList.remove('active')
      hidePermanent.classList.remove('active')
      coordCheck.classList.remove('active')
      window.narratorTools.sharedFunctions.endUnderlayNPCs()

      narratorTools.sharedFunctions.removeLock('npcSearch')
      narratorTools.sharedFunctions.removeLock('coordSearch')
    }
  }

  function selectTab (e)
  {
    const tabId = parseInt(e.target.getAttribute('tab-nr'))
    if (isNaN(tabId)) return

    changeFavouritesTo(tabId)
  }

  /* coordHelper */
  const coordHelper = {
    element: document.createElement('coordHelper'),
    x: -1,
    y: -1,
    show: function (x, y)
    {
      if (x !== '' && y !== '')
      {
        window.narratorTools.sharedFunctions.paintSquare(x, y)
        this.x = parseInt(x)
        this.y = parseInt(y)
      }
      else
      {
        this.hide()
      }
    },

    hide: function ()
    {
      window.narratorTools.sharedFunctions.removeSquare()
      this.x = -1
      this.y = -1
    }
  }

  function initiateAddNpc ()
  {
    if (settings.attachNPCToMouse)
    {
      attachNpcToMouse(input.url.value, input.name.value, input.collision.checked, confirmButton)
    }
    else
    {
      addNpc(input.x.value,
        input.y.value,
        input.url.value,
        input.name.value,
        input.collision.checked)
    }
  }

  this.draw = function ()
  {
    frame.id = 'addGraf-tab'
    frame.style.display = 'flex'

    inputFrame.id = 'add-graf__commands'
    inputFrame.className = 'commands'

    coordCheck.id = 'NT-button-coordCheck'
    coordCheck.className = 'button'
    setTip(coordCheck, 'Wybierz kolizje kliknięciem')
    coordCheck.addEventListener('click', function ()
    {
      clearSearch()
      coordCheck.classList.add('active')
      coordSearchActive = true
    })
    coordCheck.innerText = '‡'
    inputFrame.appendChild(coordCheck)

    getNpcFromMap.id = 'NT-button-getNpcFromMap'
    getNpcFromMap.className = 'button'
    setTip(getNpcFromMap, 'Skopiuj dane NPCa z mapy')
    getNpcFromMap.addEventListener('click', function ()
    {
      clearSearch()
      npcSearchType = 'get'
      getNpcFromMap.classList.add('active')
      npcSearchActive = true
      window.narratorTools.sharedFunctions.underlayNPCs('white')
    })
    getNpcFromMap.innerText = '&'
    inputFrame.appendChild(getNpcFromMap)

    hidePermanent.id = 'NT-button-hidePermanent'
    hidePermanent.className = 'button'
    setTip(hidePermanent, 'Schowaj NPCa z gry')
    hidePermanent.addEventListener('click', function ()
    {
      clearSearch()
      npcSearchType = 'hide'
      hidePermanent.classList.add('active')
      npcSearchActive = true
      window.narratorTools.sharedFunctions.underlayNPCs('black')
    })
    hidePermanent.innerText = 'X'
    inputFrame.appendChild(hidePermanent)

    input.x.id = 'NT-input-x'
    input.x.type = 'text'
    input.x.maxLength = 3
    input.x.pattern = '[0-9]*'
    input.x.addEventListener('input', lastInput.save.bind(this, 'addGraf', inputFrame), false)
    input.x.addEventListener('input', coordHelper.show.bind(this, input.x.value, input.y.value), false)
    const labelX = document.createElement('label')
    labelX.innerText = 'x:'
    labelX.htmlFor = input.x.id
    inputFrame.appendChild(labelX)
    inputFrame.appendChild(input.x)

    input.y.id = 'NT-input-y'
    input.y.type = 'text'
    input.y.maxLength = 3
    input.y.pattern = '[0-9]*'
    input.y.addEventListener('input', lastInput.save.bind(this, 'addGraf', inputFrame), false)
    input.y.addEventListener('input', coordHelper.show.bind(this, input.x.value, input.y.value), false)
    const labelY = document.createElement('label')
    labelY.innerText = 'y:'
    labelY.htmlFor = labelY.id
    inputFrame.appendChild(labelY)
    inputFrame.appendChild(input.y)

    input.url.id = 'NT-input-url'
    input.url.className = 'input-url'
    input.url.type = 'text'
    input.url.addEventListener('input', lastInput.save.bind(this, 'addGraf', inputFrame))
    const labelUrl = document.createElement('label')
    labelUrl.innerText = 'url:'
    labelUrl.htmlFor = input.url.id
    inputFrame.appendChild(labelUrl)
    inputFrame.appendChild(input.url)

    input.name.id = 'NT-input-name'
    input.name.type = 'text'
    input.name.addEventListener('input', lastInput.save.bind(this, 'addGraf', inputFrame))
    const labelName = document.createElement('label')
    labelName.innerText = 'nazwa:'
    labelName.htmlFor = input.name.id
    inputFrame.appendChild(labelName)
    inputFrame.appendChild(input.name)

    input.collision.id = 'NT-input-collision'
    input.collision.type = 'checkbox'
    input.collision.addEventListener('input', lastInput.save.bind(this, 'addGraf', inputFrame))
    const labelCollision = document.createElement('label')
    labelCollision.innerText = 'kolizja:'
    labelCollision.htmlFor = input.collision.id
    inputFrame.appendChild(labelCollision)
    inputFrame.appendChild(input.collision)

    confirmButton.id = 'NT-button-confirm'
    confirmButton.className = 'button'
    confirmButton.addEventListener('click', initiateAddNpc)
    confirmButton.innerText = 'Dodaj'
    inputFrame.appendChild(confirmButton)

    const reset = document.createElement('button')
    reset.id = 'NT-resetButton'
    reset.className = 'button'
    reset.addEventListener('click', lastInput.reset.bind(this, 'addGraf', inputFrame)) // TODO create reset function here
    reset.innerText = 'R'
    setTip(reset, 'Wyczyść wpisywane dane')
    inputFrame.appendChild(reset)

    frame.appendChild(inputFrame)

    favouriteFrame.id = 'add-graf__favourites'
    favouriteFrame.className = 'favourites'

    tabSelect.id = 'add-graf__select-tab'
    tabSelect.addEventListener('click', selectTab)
    for (let i = 0; i < 10; i++)
    {
      const button = document.createElement('button')
      button.innerText = (i + 1).toString()
      button.id = 'NT-addGraf-tabSelect-' + i
      button.className = 'tabSelect-button button'
      button.setAttribute('tab-nr', i.toString())
      setTip(
        button,
        settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC
          ? defaultTabNames[i]
          : settings.addGraf.customTabNames[i]
      )
      tabSelect.appendChild(button)
    }
    favouriteFrame.appendChild(tabSelect)

    npcSelect.id = 'select-npc'
    npcSelect.addEventListener('click', loadNpc, false) // TODO change it so it's ok in every module
    npcSelect.addEventListener('contextmenu', saveNpc, false)

    for (let i = 0; i < 10; i++)
    {
      const button = document.createElement('div')
      button.id = tabSelectButtonId + i
      button.className = 'favourites-div'
      setTipType(button, 't_npc')
      const img = document.createElement('img')
      img.className = 'favourites-img'
      button.appendChild(img)

      npcSelect.appendChild(button)
    }

    favouriteFrame.appendChild(npcSelect)
    frame.appendChild(favouriteFrame)

    const favouritesButtons = document.createElement('div')
    favouritesButtons.id = 'favourite-config-box'

    basicFavourites.id = 'NT__add-graf__favourites-buttons__basic'
    basicFavourites.className = 'favourite-config-button button'
    if (settings.addGraf.selectedType === ADD_GRAF_TYPES.BASIC) basicFavourites.classList.add('active')
    basicFavourites.innerText = 'Zwykłe'
    setTip(basicFavourites, 'Zestaw podstawowych grafik')
    basicFavourites.addEventListener('click', function ()
    {
      basicFavourites.classList.add('active')
      customFavourites.classList.remove('active')
      settings.addGraf.selectedType = ADD_GRAF_TYPES.BASIC
      saveSettings()

      for (let i = 0; i < tabSelect.children.length; i++)
      {
        setTip(tabSelect.children[i], defaultTabNames[i])
      }

      changeFavouritesTo(settings.addGraf.selectedTab)
    })

    customFavourites.id = 'NT__add-graf__favourites-buttons__custom'
    customFavourites.className = 'favourite-config-button button'
    if (settings.addGraf.selectedType === ADD_GRAF_TYPES.CUSTOM) customFavourites.classList.add('active')
    customFavourites.innerText = 'Własne'
    setTip(customFavourites, 'Zestaw własnych grafik')
    customFavourites.addEventListener('click', function ()
    {
      basicFavourites.classList.remove('active')
      customFavourites.classList.add('active')
      settings.addGraf.selectedType = ADD_GRAF_TYPES.CUSTOM
      saveSettings()

      for (let i = 0; i < tabSelect.children.length; i++)
      {
        setTip(tabSelect.children[i], settings.addGraf.customTabNames[i])
      }

      changeFavouritesTo(settings.addGraf.selectedTab)
    })

    favouritesButtons.appendChild(basicFavourites)
    favouritesButtons.appendChild(customFavourites)

    frame.appendChild(favouritesButtons)

    changeFavouritesTo(settings.addGraf.selectedTab)
    mainModuleSpace.appendChild(frame)
    lastInput.load('addGraf', inputFrame)
  }

  function loadBasicNpcs ()
  {
    loadJSON('https://glcdn.githack.com/Aphalon/narrator-tools/raw/master/basic-npcs.json?version=31', function (data)
    {
      basicNPCs = JSON.parse(data)
    })
  }

  /**
   * Initialises whole module
   * @function
   * @returns {HTMLButtonElement} Button that should be placed in nav section
   */
  this.init = function ()
  {
    loadBasicNpcs()

    window.narratorTools.sharedElements.gameWindow.addEventListener('click', searchCoords, true)
    window.narratorTools.sharedElements.gameWindow.addEventListener('click', searchNpc, true)
    document.body.addEventListener('contextmenu', clearSearch, true)

    let elmName
    if (window.narratorTools.interface === 'SI')
    {
      elmName = 'ground'
    }
    else
    {
      elmName = 'GAME_CANVAS'
    }
    document.getElementById(elmName)
      .addEventListener('mousedown', function ()
      {
        narratorTools.sharedFunctions.removeLock('coordSearch')
        narratorTools.sharedFunctions.removeLock('npcSearch')
      }, false)
    // remove lock every time //TODO: remove lock when clicking on NPC
    document.addEventListener('keydown', function ()
    {
      narratorTools.sharedFunctions.removeLock('coordSearch')
      narratorTools.sharedFunctions.removeLock('npcSearch')
    }, false)
    // lock and unlock movement if coordSearch is set

    inputFrame.addEventListener('input', function ()
    {
      updateAttachedNpcInformation(input.url.value, input.name.value, input.collision.checked)
    })

    const navButton = document.createElement('button')
    navButton.id = 'open-addGraf'
    setTip(navButton, 'Ustawienia własnych NPCów')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    const navImage = document.createElement('img')
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/npc/mez/npc04.gif'
    navButton.appendChild(navImage)

    return navButton
  }
}()
