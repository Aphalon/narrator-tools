import { displayModule, mainModuleSpace } from '../main-panel'
import { changeMap, resetMap } from '../chat'
import { loadFavourite, saveFavourite, updateSingleCell } from '../favourites'
import lastInput from '../last-input'
import { checkId } from '../helper-functions'
import { setTip, setTipType } from '../tips'

export default new function ()
{
  this.name = 'map'
  this.orderId = 20

  const frame = document.createElement('div')
  const inputFrame = document.createElement('div')
  const mapPreview = document.createElement('img')
  const favouriteFrame = document.createElement('div')

  const input = {
    url: document.createElement('input')
  }

  const mapSelectButtonId = 'NT-map-configButton-'

  const customMaps = []

  function updatePreview ()
  {
    mapPreview.src = input.url.value
  }

  function loadCustomMaps ()
  {
    const maxCustomMaps = 10
    for (let i = 0; i < maxCustomMaps; i++)
    { // todo change it to loadFavourite
      const data = window.localStorage.getItem('narratorTools-customMap-' + i)
      if (data)
      {
        const parsedData = JSON.parse(data)
        customMaps[i] = {
          url: parsedData.url
        }
      }
    }
  }

  function initChangeMap ()
  {
    const mapUrl = input.url.value
    changeMap(mapUrl)
  }

  function loadMap (e)
  {
    const id = checkId(e)
    const map = customMaps[id]
    if (map)
    {
      input.url.value = map.url
      updatePreview()
    }
  }

  function saveMap (e)
  {
    e.preventDefault()
    const id = checkId(e)
    const obj = {
      url: input.url.value,
      name: input.url.value
    }
    customMaps[id] = obj
    saveFavourite('customMap', id, obj)
    updateSingleCell(e.target, obj)
  }

  this.draw = function ()
  {
    loadCustomMaps()

    frame.id = 'map-tab'
    frame.style.display = 'flex'

    inputFrame.id = 'map__commands'
    inputFrame.className = 'commands'

    input.url.id = 'NT-input-map'
    input.url.className = 'input-url'
    input.url.type = 'text'
    input.url.addEventListener('input', lastInput.save.bind(this, 'map', inputFrame), false)
    input.url.addEventListener('input', updatePreview, false)
    input.url.value = window.localStorage.getItem('addGrafCurrentMap')
    const labelUrl = document.createElement('label')
    labelUrl.innerText = 'Link do mapy:'
    labelUrl.htmlFor = input.url.id
    inputFrame.appendChild(labelUrl)
    inputFrame.appendChild(input.url)

    const confirmButton = document.createElement('button')
    confirmButton.id = 'NT-map-button-confirm'
    confirmButton.className = 'button'
    confirmButton.addEventListener('click', initChangeMap)
    confirmButton.innerText = 'Zmień mapę'
    inputFrame.appendChild(confirmButton)

    const resetMapButton = document.createElement('button')
    resetMapButton.id = 'NT-map-button-resetMap'
    resetMapButton.className = 'button'
    resetMapButton.addEventListener('click', resetMap)
    resetMapButton.innerText = 'Resetuj mapę'
    inputFrame.appendChild(resetMapButton)

    const previewContainer = document.createElement('div')
    previewContainer.id = 'NT-map-preview'
    mapPreview.alt = 'Mapa zawiera błędy lub nie wczytano żadnej.'
    previewContainer.appendChild(mapPreview)
    inputFrame.appendChild(previewContainer)

    frame.appendChild(inputFrame)

    favouriteFrame.id = 'map__favourites'
    favouriteFrame.className = 'favourites'

    const mapSelect = document.createElement('div')
    mapSelect.addEventListener('click', loadMap, false)
    mapSelect.addEventListener('contextmenu', saveMap, false)

    favouriteFrame.appendChild(mapSelect)

    for (let i = 0; i < 10; i++)
    {
      const button = document.createElement('div')
      button.id = mapSelectButtonId + i
      button.className = 'favourites-div'
      setTipType(button, 't_npc')
      const img = document.createElement('img')
      img.className = 'favourites-img'
      button.appendChild(img)

      const data = loadFavourite('customMap', i)
      updateSingleCell(button, data)

      mapSelect.appendChild(button)
    }

    frame.appendChild(favouriteFrame)

    mainModuleSpace.appendChild(frame)
    lastInput.load('map', inputFrame)
    updatePreview()
  }

  this.init = function ()
  {
    const navButton = document.createElement('button')
    navButton.id = 'open-map'
    setTip(navButton, 'Ustawienia mapy')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    const navImage = document.createElement('img')
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/itemy/que/mapa_cala.gif'
    navButton.appendChild(navImage)

    return navButton
  }
}()
