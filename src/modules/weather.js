import { displayModule, mainModuleSpace } from '../main-panel'
import { changeWeather } from '../chat'
import { setTip } from '../tips'

export default new function ()
{
  this.name = 'weather'
  this.orderId = 40

  const frame = document.createElement('div')
  const navImage = document.createElement('img')

  const gameWeatherImages = {
    fish: 'https://micc.garmory-cdn.cloud/obrazki/itemy/kon/flemona.gif',
    light: 'https://micc.garmory-cdn.cloud/obrazki/npc/obj/magic_dust2.gif',
    latern: 'https://micc.garmory-cdn.cloud/obrazki/itemy/kon/ark-lamp-kol.gif',
    bat: 'https://micc.garmory-cdn.cloud/obrazki/itemy/sur/bat_wing01.gif'
  }

  const weatherNames = {
    reset: 'Reset',
    'clear-day': 'Czyste niebo',
    'clear-night': 'Bezchmurna noc',
    'day-cloud-big': 'Duża ilość chmur',
    'day-cloud-small': 'Niewielka ilość chmur',
    'day-rain': 'Słaby deszcz',
    'day-rain-with-snow': 'Deszcz ze śniegiem',
    'day-snow': 'Śnieżyca',
    'day-storm': 'Burza',
    'night-cloud-big': 'Chmurna noc',
    'night-cloud-small': 'Noc z małym zachmurzeniem',
    'night-rain': 'Lekki nocny deszcz',
    'night-rain-with-snow': 'Nocny deszcz ze śniegiem',
    'rain-with-snow': 'Ciężki deszcz ze śniegiem',
    'night-snow': 'Nocna śnieżyca',
    'night-storm': 'Nocna burza',
    overcast: 'Niebo przykryte chmurami',
    rain: 'Ciężki deszcz',
    'rain-light': 'Słaby deszcz bez słońca',
    snow: 'Ciężka śnieżyca',
    'snow-storm': 'Burza śnieżna',
    storm: 'Ciężka burza',
    fish: 'Pod wodą',
    light: 'Światełka',
    latern: 'Lampiony',
    bat: 'Hallowen'
  }

  function updateButton (name)
  {
    console.log('weatherName is: ' + name)
    switch (name)
    {
      case 'indoor':
        navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/itemy/pap/pap42.gif'
        break
      case 'fish':
        navImage.src = gameWeatherImages.fish
        break
      case 'light':
        navImage.src = gameWeatherImages.light
        break
      case 'latern':
        navImage.src = gameWeatherImages.latern
        break
      case 'bat':
        navImage.src = gameWeatherImages.bat
        break
      default:
        navImage.src = NERTHUS_FILE_PREFIX + 'res/img/weather/icons/' + name + '.png'
    }
  }

  function markWeather (e)
  {
    e.preventDefault()
    // const id = checkId(e);
    e.target.parentElement.classList.toggle('marked')
  }

  this.draw = function ()
  {
    frame.id = 'weather-tab'

    for (const weather in weatherNames)
    {
      const button = document.createElement('button')
      button.id = 'NT-weather-button-' + weather
      button.setAttribute('data-weather-name', weather)
      button.className = 'weather-div'
      setTip(button, weatherNames[weather])
      const image = document.createElement('div')
      image.id = 'NT-weather-image-' + weather
      image.className = 'weather-img'
      if (['reset', 'fish', 'light', 'latern', 'bat'].includes(weather))
      {
        image.classList.add('game-weather')
        switch (weather)
        {
          case 'reset':
            image.style.backgroundImage = 'url(https://micc.garmory-cdn.cloud/obrazki/itemy/pap/pap67.gif)'
            break
          case 'fish':
            image.style.backgroundImage = `url(${gameWeatherImages.fish})`
            break
          case 'light':
            image.style.backgroundImage = `url(${gameWeatherImages.light})`
            break
          case 'latern':
            image.style.backgroundImage = `url(${gameWeatherImages.latern})`
            break
          case 'bat':
            image.style.backgroundImage = `url(${gameWeatherImages.bat})`
            break
        }
      }
      else
      {
        image.style.backgroundImage = 'url(' + NERTHUS_FILE_PREFIX + 'res/img/weather/icons/' + weather + '.png)'
      }
      button.appendChild(image)
      frame.appendChild(button)
    }
    frame.addEventListener('click', function (e)
    {
      if (e.target.className === 'weather-div')
      {
        changeWeather(e.target.getAttribute('data-weather-name'))
      }
    }, false)
    frame.addEventListener('contextmenu', markWeather, false)

    mainModuleSpace.appendChild(frame)
  }

  this.init = function ()
  {
    const navButton = document.createElement('button')
    navButton.id = 'open-weather'
    setTip(navButton, 'Ustawienia pogody')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    navImage.id = 'open-weather-img'
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/itemy/pap/pap42.gif'
    updateButton(nerthus.weather.getWeather(new Date()).name)
    navButton.appendChild(navImage)

    nerthus.addCallbackToEvent('displayWeather', updateButton)

    return navButton
  }
}()
