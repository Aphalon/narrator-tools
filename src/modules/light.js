import { displayModule, mainModuleSpace } from '../main-panel'
import { changeLight, resetLight } from '../chat'
import { setTip } from '../tips'

export default new function ()
{
  this.name = 'light'
  this.orderId = 30

  const frame = document.createElement('div')
  const lightRange = document.createElement('input')
  const colorPicker = document.createElement('input')

  let savedLightOpacity = -1
  let savedLightColor = '#000'

  function saveLightVal ()
  {
    savedLightOpacity = nerthus.night.currentOpacity
    savedLightColor = nerthus.night.currentColor
    document.getElementById('nerthus-night').style.transition = 'none'
  }

  function revertLight ()
  {
    nerthus.night.changeLight(savedLightOpacity, savedLightColor)
    document.getElementById('nerthus-night').style.transition = ''
  }

  function initChangeLight ()
  {
    const lightValue = parseFloat(lightRange.value) / 100
    changeLight(lightValue, colorPicker.value)
  }

  this.draw = function ()
  {
    frame.id = 'light-tab'

    lightRange.id = 'NT-map-input-light'
    lightRange.type = 'range'
    lightRange.max = '100'
    lightRange.min = '0'
    lightRange.defaultValue = '100'
    setTip(lightRange, 'Przezroczystość światła: 1')

    lightRange.addEventListener('input', function ()
    {
      const value = lightRange.value / 100
      const valueString = (value).toString()
      setTip(lightRange, `Przezroczystość światła: ${valueString}`)
      const color = colorPicker.value

      nerthus.night.changeLight(1 - value, color, true)
    })

    lightRange.addEventListener('mousedown', saveLightVal)
    lightRange.addEventListener('mouseup', revertLight)

    frame.appendChild(lightRange)

    colorPicker.className = 'color-picker'
    colorPicker.type = 'color'
    colorPicker.value = '#00000'
    setTip(colorPicker, 'Kolor światła')

    frame.appendChild(colorPicker)

    const confirmLight = document.createElement('button')
    confirmLight.id = 'NT-map-button-light'
    confirmLight.className = 'button'
    confirmLight.addEventListener('click', initChangeLight, false)
    confirmLight.innerText = 'Zmień światło'
    frame.appendChild(confirmLight)

    const resetLightButton = document.createElement('button')
    resetLightButton.id = 'NT-map-button-reset-light'
    resetLightButton.className = 'button'
    resetLightButton.addEventListener('click', resetLight, false)
    resetLightButton.innerText = 'Resetuj światło'
    frame.appendChild(resetLightButton)

    mainModuleSpace.appendChild(frame)
  }

  this.init = function ()
  {
    const navButton = document.createElement('button')
    navButton.id = 'open-light'
    setTip(navButton, 'Ustawienia światła')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    const navImage = document.createElement('img')
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/npc/map/swieczka01.gif'
    navButton.appendChild(navImage)

    return navButton
  }
}()
