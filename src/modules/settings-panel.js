import { displayModule, frameElement, mainModuleSpace } from '../main-panel'
import { saveSettings, settings } from '../settings'
import { revokeDraggable, setDraggable } from '../dragging'
import { loadStyle } from '../style'
import { setTip } from '../tips'

export default new function ()
{
  this.name = 'settings'
  this.orderId = 100

  const frame = document.createElement('div')

  const helpString = 'Lewy przycisk myszy na przycisku ulubionych wybiera danego NPCa czy też mapę i uzupełnia nim formularz. ' +
    'By je ustawić, należy wybrać ewentualne opcje typu pozycja X i Y w wypadku NPCa i nacisnąć przycisk \'dodaj\'<hr>' +
    'By zapisać swoją mapę czy też NPCa w zakładce \'własne\' wystarczy wpisać jego dane w formularz, a następnie kliknąć ' +
    'prawym przyciskiem myszy na wolne pole.<hr>' +
    'By usunąć NPCa z ulubionych wystarczy nadpisać go pustym formularzem. Najprostszym sposobem jest najpierw ' +
    'kliknięcie przycisku R (reset wpisywanych danych), a następnie prawym przyciskiem na zapisanego NPCa do usunięcia<hr>' +
    'By zwinąć lub ponownie otworzyć dodatek wystarczy  kliknąć <a href=\'https://i.imgur.com/jzcXSIz.png\' target=\'_blank\'>przycisk koordynatów.</a><hr>' +
    'Wstawienie NPCa z pustym polem URL doda jedynie nazwę i/lub kolizję do wybranego pola na mapie.<hr>' +
    'W razie problemów można pisać do Kris Aphalon#3484 na Discordzie bądź na ten sam nick w grze.'

  function showHelp ()
  {
    window.mAlert(helpString)
  }

  function resetPosition ()
  {
    frameElement.style.top = ''
    frameElement.style.left = ''
    settings.position.top = ''
    settings.position.left = ''
    saveSettings()
  }

  function changeDraggableSetting (e)
  {
    if (e.target.checked)
    {
      setDraggable(frameElement)
      settings.draggable = true
      saveSettings()
    }
    else
    {
      revokeDraggable(frameElement)
      settings.draggable = false
      saveSettings()
    }
  }

  function changeVerticalSetting (e)
  {
    // todo
    e.target.checked ? loadStyle('vertical') : loadStyle('horizontal')
  }

  function changeNpcAddingSetting (e)
  {
    settings.attachNPCToMouse = e.target.checked
    saveSettings()
  }

  function changeRecordingPromptSetting (e)
  {
    settings.recordingPrompt = e.target.checked
    saveSettings()
  }

  function changeRecordGlobalChatSetting (e)
  {
    settings.recordGlobalChatChannel = e.target.checked
    saveSettings()
  }

  this.draw = function ()
  {
    frame.id = 'settings-tab'
    frame.style.display = 'flex'

    const buttonFrame = document.createElement('div')
    buttonFrame.className = 'button-frame'

    const resetPositionButton = document.createElement('button')
    resetPositionButton.onclick = resetPosition
    resetPositionButton.innerText = 'Reset pozycji'
    resetPositionButton.className = 'button'
    const resetPositionFrame = buttonFrame.cloneNode()
    resetPositionFrame.appendChild(resetPositionButton)
    frame.appendChild(resetPositionFrame)

    const draggableFrame = document.createElement('div')
    draggableFrame.className = 'checkbox-frame'

    const draggableCheckbox = document.createElement('input')
    draggableCheckbox.id = 'draggable-checkbox'
    draggableCheckbox.type = 'checkbox'
    draggableCheckbox.checked = settings.draggable
    draggableCheckbox.addEventListener('change', changeDraggableSetting, false)
    const labelDraggable = document.createElement('label')
    labelDraggable.htmlFor = draggableCheckbox.id
    labelDraggable.innerText = 'Przesuwanie okna'
    labelDraggable.style.fontSize = '14px'
    setTip(draggableFrame, 'By przesuwać okno złap za brzeg ramki')
    draggableFrame.appendChild(labelDraggable)
    draggableFrame.appendChild(draggableCheckbox)

    frame.appendChild(draggableFrame)

    const verticalFrame = document.createElement('div')
    verticalFrame.className = 'checkbox-frame'

    const verticalCheckbox = document.createElement('input')
    verticalCheckbox.id = 'vertical-checkbox'
    verticalCheckbox.type = 'checkbox'
    verticalCheckbox.checked = settings.style === 'vertical'
    verticalCheckbox.addEventListener('change', changeVerticalSetting, false)
    const labelVertical = document.createElement('label')
    labelVertical.htmlFor = verticalCheckbox.id
    labelVertical.innerText = 'Tryb pionowy'
    verticalFrame.appendChild(labelVertical)
    verticalFrame.appendChild(verticalCheckbox)

    frame.appendChild(verticalFrame)

    const npcAddingFrame = document.createElement('div')
    npcAddingFrame.className = 'checkbox-frame'
    const npcAddingTip = `Zmienia sposób, w jaki działa przycisk 'dodaj' w zakładce dodawania NPCów.
<hr>
Przy zaznaczonej opcji po naciśnięciu przycisku 'dodaj' NPCe będą się przyklejały do myszki.
<hr>
Przy opcji odznaczonej, NPC doda się na koordynaty wpisane w pola X i Y`
    setTip(npcAddingFrame, npcAddingTip)

    const npcAddingCheckbox = document.createElement('input')
    npcAddingCheckbox.id = 'npc-adding-checkbox'
    npcAddingCheckbox.type = 'checkbox'
    npcAddingCheckbox.checked = settings.attachNPCToMouse
    npcAddingCheckbox.addEventListener('change', changeNpcAddingSetting, false)
    const labelNpcAdding = document.createElement('label')
    labelNpcAdding.htmlFor = npcAddingCheckbox.id
    labelNpcAdding.innerText = 'Lift&Drop'
    npcAddingFrame.appendChild(labelNpcAdding)
    npcAddingFrame.appendChild(npcAddingCheckbox)

    frame.appendChild(npcAddingFrame)

    const recordingPromptFrame = document.createElement('div')
    recordingPromptFrame.className = 'checkbox-frame'

    const recordingPromptCheckbox = document.createElement('input')
    recordingPromptCheckbox.id = 'recording-prompt-checkbox'
    recordingPromptCheckbox.type = 'checkbox'
    recordingPromptCheckbox.checked = settings.recordingPrompt
    recordingPromptCheckbox.addEventListener('change', changeRecordingPromptSetting, false)
    const labelRecordingPrompt = document.createElement('label')
    labelRecordingPrompt.htmlFor = recordingPromptCheckbox.id
    labelRecordingPrompt.innerText = 'Prompt o nagrywanie'
    labelRecordingPrompt.style.fontSize = '14px'
    recordingPromptFrame.appendChild(labelRecordingPrompt)
    recordingPromptFrame.appendChild(recordingPromptCheckbox)
    setTip(
      recordingPromptFrame,
      'Wyświetlaj wiadomość o nagrywaniu za pierwszym razem, kiedy zauważysz komendę na czacie.'
    )

    frame.appendChild(recordingPromptFrame)

    const recordGlobalChatFrame = document.createElement('div')
    recordGlobalChatFrame.className = 'checkbox-frame'

    const recordGlobalChatCheckbox = document.createElement('input')
    recordGlobalChatCheckbox.id = 'record-global-chat-checkbox'
    recordGlobalChatCheckbox.type = 'checkbox'
    recordGlobalChatCheckbox.checked = settings.recordGlobalChatChannel
    recordGlobalChatCheckbox.addEventListener('change', changeRecordGlobalChatSetting, false)
    const recordGlobalChatLabel = document.createElement('label')
    recordGlobalChatLabel.htmlFor = recordGlobalChatCheckbox.id
    recordGlobalChatLabel.innerText = 'Nagrywaj czat globalny'
    recordGlobalChatLabel.style.fontSize = '13px'
    recordGlobalChatFrame.appendChild(recordGlobalChatLabel)
    recordGlobalChatFrame.appendChild(recordGlobalChatCheckbox)
    setTip(
      recordGlobalChatFrame,
      'Przy nagrywaniu, nagrywaj również wiadomości na czacie globalnym.'
    )

    frame.appendChild(recordGlobalChatFrame)

    const helpButton = document.createElement('button')
    helpButton.onclick = showHelp
    helpButton.innerText = 'POMOC'
    helpButton.className = 'button'
    const helpFrame = buttonFrame.cloneNode()
    helpFrame.appendChild(helpButton)
    frame.appendChild(helpFrame)

    mainModuleSpace.appendChild(frame)
  }

  this.init = function ()
  {
    const navButton = document.createElement('button')
    navButton.id = 'open-settings'
    setTip(navButton, 'Ustawienia')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    const navImage = document.createElement('img')
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/itemy/neu/mechg-czesci.gif'
    navButton.appendChild(navImage)

    return navButton
  }
}()
