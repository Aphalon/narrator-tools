import { displayModule, mainModuleSpace } from '../main-panel'
import { addToChatChecklist } from '../chat'
import { setTip } from '../tips'

export default new function ()
{
  this.name = 'advancedWorldEdit'
  this.orderId = 70

  const frame = document.createElement('div')

  let npcList = []

  function Npc (x, y, url, name, collision)
  {
    this.x = parseInt(x)
    this.y = parseInt(y)
    this.url = url
    this.name = name
    this.collision = collision
  }

  function Delete (id)
  {
    // TODO ISSUE: when loading old ones it returns empty object
    const npc = window.narratorTools.sharedFunctions.getNpcData(id)
    if (npc)
    {
      this.type = 'delete'
      this.id = parseInt(id)
      this.lvl = npc.lvl
    }
  }

  function saveNpcList (saveType) // false: for session only
  {
    const storage = saveType ? window.localStorage : window.sessionStorage
    storage.setItem('NarratorTools-NpcList', JSON.stringify(npcList))
  }

  function loadNpcList (saveType) // false: from session
  {
    const storage = saveType ? window.localStorage : window.sessionStorage
    return JSON.parse(storage.getItem('NarratorTools-NpcList') || '[]')
  }

  function purgeNpcList ()
  {
    npcList.length = 0
    saveNpcList(false)
  }

  function duplicateInNpcList (npc)
  {
    const len = npcList.length
    for (let i = 0; i < len; i++)
    {
      if (npcList[i].x === npc.x &&
        npcList[i].y === npc.y &&
        npcList[i].url === npc.url &&
        npcList[i].name === npc.name &&
        npcList[i].collision === npc.collision
      )
      {
        return true
      }
    }
    return false
  }

  function parseAddGraf (cmd)
  {
    cmd = cmd.split(',')
    const npc = new Npc(cmd[0], cmd[1], cmd[3], cmd[2], !!Number(cmd[4]))
    if (!duplicateInNpcList(npc))
    {
      npcList.push(npc)
      saveNpcList(false)
    }
  }

  function parseDelGraf (cmd)
  {
    cmd = cmd.split(',')

    // don't break references with Array.filter
    let i = npcList.length
    while (i--)
    {
      if (npcList[i].x === parseInt(cmd[0]) && npcList[i].y === parseInt(cmd[1]))
      {
        npcList.splice(i, 1)
      }
    }
    saveNpcList(false)
  }

  function parseHide (cmd)
  {
    const obj = new Delete(cmd)
    if (typeof obj.type !== 'undefined')
    {
      npcList.push(obj)
      saveNpcList(false)
    }
  }

  function downloadNpcList ()
  {
    const a = window.document.createElement('a')
    a.href = window.URL.createObjectURL(new window.Blob([JSON.stringify(npcList, null, 4)], { type: 'text/json' }))
    a.download = 'map_' + window.narratorTools.sharedElements.mapId + '.json'
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  }

  // only SI
  function addAdvancedOptionsToNpcs ()
  {
    const npcs = document.querySelectorAll('#base > img[class="nerthus-npc"]')
    const len = npcs.length
    for (let i = 0; i < len; i++)
    {
      const id = npcs[i].id.slice(3)
      console.log(id)
      let npc, x, y, name
      if (window.nerthus.npc.list[id])
      {
        const customNpc = window.nerthus.npc.list[id]
        npc = new Npc(customNpc.x, customNpc.y, customNpc.url, customNpc.name, customNpc.collision)
        x = npc.x
        y = npc.y
        name = npc.name
      }
      else
      {
        console.log(id)
        for (const npcArr of window.nerthus.worldEdit.npcs)
        {
          if (50000000 + (npcArr[1] * 1000) + npcArr[2] === parseInt(id))
          {
            x = npcArr[1] // TODO on NI it will be one lower
            y = npcArr[2]
            name = npcArr[4]
            npc = new Npc(x, y, npcArr[3], name, !!Number(npcArr[5]))

            break
          }
        }
      }
      if (npc)
      {
        npcList.push(npc)
        console.log(x, y, name)
        npcs[i].onclick = function (e)
        {
          window.showMenu(e,
            [
              ['Testuj dialog', 'narratorTools.advancedWorldEdit.testTalkWindow("' + name + '",' + x + ',' + y + ')'],
              ['Edytuj dialog', 'narratorTools.advancedWorldEdit.editTalkWindow("' + name + '",' + x + ',' + y + ')']
            ],
            true)
        }
      }
    }
    console.log(npcs)
  }

  function getNpcDialog (npcX, npcY, npcName)
  {
    for (const npc of npcList)
    {
      if (npc.name === npcName && npc.x === npcX && npc.y === npcY && npc.dialog)
      {
        return npc.dialog
      }
    }
    return false
  }

  /*
  class Dialog
  {
    constructor (npcName, npcText, playerResponses = {})
    {
      this.npcName = npcName
      this.npcText = npcText
      this.responses = playerResponses
    }

    getParsedDialog (id)
    {
      const parsedDialog = [0, this.npcName, 0, 1, this.responses[id].text, '']
      return
    }

    addResponses (id, npcText, playerResponses)
    {
      this.responses[id] = [npcText, playerResponses]
    }
  }

  class PlayerResponse
  {
    constructor (type, text, goto = -1)
    {
      this.type = type
      this.text = text
      this.goto = goto
    }
  }
   */

  function testTalkWindow (npcName, npcX, npcY, dialNr = 0)
  {
    console.log('test talk window yeaa')
    console.log(npcName, npcX, npcY, dialNr)

    const dialog = getNpcDialog(npcX, npcY, npcName)
    console.log(dialog)
    if (dialog)
    {
      const parsedDialog = [0, npcName, 0, 1, dialog[dialNr][0], '']
      const len = dialog[dialNr].length
      let changeIcon = false
      if (len <= 2)
      {
        parsedDialog.push(2) // 2 - add normal option, only 1 end option is bugged
        const optionArr = /(.*)(?: ->)(.*)/g.exec(dialog[dialNr][1])
        if (optionArr[2] === 'END')
        {
          changeIcon = true
          parsedDialog.push(optionArr[1])
          parsedDialog.push(0)
        }
        else
        {
          parsedDialog.push(optionArr[1])
          parsedDialog.push(Number(optionArr[2]))
        }
      }
      else
      {
        for (let i = 1; i < len; i++)
        {
          console.log(dialog[dialNr][i])
          const optionArr = /(.*)(?: ->)(.*)/g.exec(dialog[dialNr][i])
          console.log(optionArr)
          if (optionArr[2] === 'END')
          {
            parsedDialog.push(6) // 6 - koniec
            parsedDialog.push(optionArr[1])
            parsedDialog.push(0)
          }
          else
          {
            parsedDialog.push(2) // 2 - normalna
            parsedDialog.push(optionArr[1])
            parsedDialog.push(Number(optionArr[2]))
          }
        }
      }
      console.log(parsedDialog)
      window.npcTalk(parsedDialog)

      const replies = document.getElementById('replies')
      if (changeIcon)
      {
        replies.childNodes[0].className = 'icon icon LINE_EXIT'
        replies.childNodes[0].childNodes[0].className = 'icon icon LINE_EXIT'
        replies.childNodes[0].onclick = function ()
        {
          window.npcTalk(['4', '', ''])
          document.getElementById('dialog').style.display = 'none'
          window.map.resizeView()
        }
      }
      else
      {
        const repliesLen = replies.childNodes.length
        for (let i = 0; i < repliesLen; i++)
        {
          const j = (i * 3) + 6
          if (parsedDialog[j] === 6)
          {
            replies.childNodes[i].onclick = function ()
            {
              window.npcTalk(['4', '', ''])
              document.getElementById('dialog').style.display = 'none'
              window.map.resizeView()
            }
          }
          else if (parsedDialog[j] === 2)
          {
            replies.childNodes[i].onclick = function ()
            {
              console.log('going to' + parsedDialog[j + 2])
              testTalkWindow(npcName, npcX, npcY, parsedDialog[j + 2])
            }
          }
        }
      }
      return true
    }
    else
    {
      window.message('Ten NPC nie ma dialogu')
      return false
    }
  }

  function editTalk (npcName, npcX, npcY, dialog, onlyOne = false, dialNr = 0)
  {
    const len = npcList.length
    for (let i = 0; i < len; i++)
    {
      if (npcList[i].name === npcName &&
        npcList[i].x === npcX &&
        npcList[i].y === npcY)
      {
        let dialogObj = {}

        if (onlyOne)
        {
          if (npcList[i].dialog)
          {
            dialogObj = npcList[i].dialog
          }
          console.log(dialogObj[dialNr])
          dialogObj[dialNr.toString()] = []
          dialogObj[dialNr.toString()][0] = dialog[0]
          const len2 = dialog.length
          for (let k = 1; k < len2; k++)
          {
            dialogObj[dialNr.toString()].push(dialog[k][0] + ' ->' + dialog[k][1])
          }
          npcList[i].dialog = dialogObj
        }
        else
        {
          const len2 = dialog.length
          for (let j = 0; j < len2; j++)
          {
            dialogObj[j.toString()] = [dialog[j][0]]
            console.log(dialogObj)
            const len3 = dialog[j].length
            for (let k = 1; k < len3; k++)
            {
              dialogObj[j.toString()].push(dialog[j][k][0] + ' ->' + dialog[j][k][1])
            }
          }
        }

        npcList[i].dialog = dialogObj
        saveNpcList(false)
        return true
      }
    }
  }

  function editTalkWindow (npcName, npcX, npcY, dialNr = 0)
  {
    console.log('edit talk window yeaa')
    console.log(npcName, npcX, npcY, dialNr)

    const len = npcList.length
    for (let i = 0; i < len; i++)
    {
      if (npcList[i].name === npcName &&
        npcList[i].x === npcX &&
        npcList[i].y === npcY)
      {
        if (!npcList[i].dialog)
        {
          npcList[i].dialog = { 0: ['Dialog', 'Opcja ->END'] }
        }
        const dialog = npcList[i].dialog
        // console.log("To jest zmienna o nazwie 'dialog'");
        // console.log(dialog);
        // console.log("---------------------------------");
        const parsedDialog = [0, npcName, 0, 1, '<textarea id=\'editMainMessage\' style=\'width: 486px;\'>' + dialog[dialNr][0] + '</textarea>', '']
        const len = dialog[dialNr].length
        for (let j = 1; j < len; j++)
        {
          // console.log("To jest zmienna o nazwie 'dialog[dialNr][i]'");
          // console.log(dialog[dialNr][i]);
          // console.log("---------------------------------");
          const optionArr = /(.*)(?: ->)(.*)/g.exec(dialog[dialNr][j])
          if (optionArr[2] === 'END')
          {
            parsedDialog.push(6) // 6 - koniec
            parsedDialog.push(
              '<input id=\'editResponseMessage-' + j + '\'  value=\'' + optionArr[1] + '\' class="editResponseMessage"\'>' +
              '<input id=\'editResponseTarget-' + j + '\'  value=\'' + optionArr[2] + '\' class="editResponseTarget"\'>' +
              '<button onclick=\'let li = event.target.parentNode; li.parentNode.removeChild(li)\' tip=\'Usuń opcję dialogową\'></button>'
            )
            parsedDialog.push(0)
          }
          else
          {
            parsedDialog.push(2) // 2 - normalna
            parsedDialog.push(
              '<input id=\'editResponseMessage-' + j + '\'  value=\'' + optionArr[1] + '\' class="editResponseMessage"\'>' +
              '<input id=\'editResponseTarget-' + j + '\'  value=\'' + optionArr[2] + '\' class="editResponseTarget"\'>' +
              '<button onclick=\'let li = event.target.parentNode; li.parentNode.removeChild(li)\' tip=\'Usuń opcję dialogową\'></button>'
            )
            parsedDialog.push(Number(optionArr[2]))
          }
        }
        window.npcTalk(parsedDialog)

        // edit buttons
        const dialogWindow = document.getElementById('dialog')

        const dialogNumbers = document.createElement('div')
        const saveButton = document.createElement('button')

        saveButton.onclick = function ()
        {
          const mainText = document.getElementById('editMainMessage').value
          const li = dialogWindow.querySelectorAll('li')
          // console.log(dialogWindow.querySelectorAll("li"));
          const dialog = [
            mainText
          ]
          const len = li.length
          for (let i = 0; i < len; i++)
          {
            dialog.push([li[i].childNodes[1].value, li[i].childNodes[2].value])
          }
          // console.log(dialog);
          editTalk(npcName, npcX, npcY, dialog, true, dialNr)
        }
        saveButton.innerText = 'Zapisz'
        saveButton.id = 'save'
        const exitButton = document.createElement('button')
        exitButton.onclick = function ()
        {
          window.npcTalk(['4', '', ''])
          document.getElementById('dialog').style.display = 'none'
          window.map.resizeView()
          dialogWindow.removeChild(saveButton)
          dialogWindow.removeChild(exitButton)
          replies.removeChild(moreResponsesButton)
          dialogWindow.removeChild(dialogNumbers)
        }
        exitButton.innerText = 'Wyjdź'
        exitButton.id = 'exit'

        const replies = document.getElementById('replies')
        const moreResponsesButton = document.createElement('button')
        moreResponsesButton.onclick = function ()
        {
          const newLi = document.createElement('li')
          const lineOptionIcon = document.createElement('div')
          lineOptionIcon.className = 'icon icon LINE_OPTION'
          const editResponseMessage = document.createElement('input')
          editResponseMessage.id = 'editResponseMessage'
          const editResponseTarget = document.createElement('input')
          editResponseTarget.id = 'editResponseTarget'
          const deleteResponse = document.createElement('button')
          setTip(deleteResponse, 'Usuń opcję dialogową')
          deleteResponse.onclick = function (e)
          {
            const li = e.target.parentNode
            li.parentNode.removeChild(li)
          }
          newLi.appendChild(lineOptionIcon)
          newLi.appendChild(editResponseMessage)
          newLi.appendChild(editResponseTarget)
          newLi.appendChild(deleteResponse)

          replies.insertBefore(newLi, moreResponsesButton)
        }
        moreResponsesButton.innerText = '+'

        const canvas = document.createDocumentFragment()
        canvas.appendChild(saveButton)
        canvas.appendChild(exitButton)
        replies.appendChild(moreResponsesButton)
        dialogWindow.appendChild(canvas)

        const dialLen = Object.keys(dialog).length
        for (let j = 0; j < dialLen; j++)
        { // todo change to one event listener
          const button = document.createElement('button')
          button.addEventListener('click', function ()
          {
            window.npcTalk(['4', '', ''])
            dialogWindow.removeChild(saveButton)
            dialogWindow.removeChild(exitButton)
            replies.removeChild(moreResponsesButton)
            dialogWindow.removeChild(dialogNumbers)
            console.log('changing dialog tab to: ' + j)

            const mainText = document.getElementById('editMainMessage').value
            const li = dialogWindow.querySelectorAll('li')
            console.log(dialogWindow.querySelectorAll('li'))
            const dialog = [
              mainText
            ]
            const len = li.length
            for (let i = 0; i < len; i++)
            {
              dialog.push([li[i].childNodes[1].value, li[i].childNodes[2].value])
            }
            console.log(dialog)
            editTalk(npcName, npcX, npcY, dialog, true, dialNr)

            editTalkWindow(npcName, npcX, npcY, j)
          })
          button.innerText = j.toString()
          dialogNumbers.appendChild(button)
        }
        const button = document.createElement('button')
        button.addEventListener('click', function ()
        {
          window.npcTalk(['4', '', ''])
          dialogWindow.removeChild(saveButton)
          dialogWindow.removeChild(exitButton)
          replies.removeChild(moreResponsesButton)
          dialogWindow.removeChild(dialogNumbers)

          const mainText = document.getElementById('editMainMessage').value
          const li = dialogWindow.querySelectorAll('li')
          console.log(dialogWindow.querySelectorAll('li'))
          const dialog = [
            mainText
          ]
          const len = li.length
          for (let i = 0; i < len; i++)
          {
            dialog.push([li[i].childNodes[1].value, li[i].childNodes[2].value])
          }
          console.log(dialog)
          editTalk(npcName, npcX, npcY, dialog, true, dialNr)

          npcList[i].dialog[dialLen.toString()] = ['Dialog', 'Opcja ->END']
          editTalkWindow(npcName, npcX, npcY, dialLen)
        })
        button.innerText = '+'
        dialogNumbers.appendChild(button)
        dialogWindow.prepend(dialogNumbers)
        const repliesLen = replies.childNodes.length
        for (let i = 0; i < repliesLen; i++)
        {
          const j = (i * 3) + 6
          if (parsedDialog[j] === 6)
          {
            replies.childNodes[i].onclick = function ()
            {
              // window.npcTalk(["4", "", ""]);
              // document.getElementById("dialog").style.display = "none";
              // map.resizeView();
            }
          }
          else if (parsedDialog[j] === 2)
          {
            replies.childNodes[i].onclick = function ()
            {
              // console.log("going to" + parsedDialog[j + 2]);
              // narratorTools.advancedWorldEdit.editTalkWindow(npcName, npcX, npcY, parsedDialog[j + 2]);
            }
          }
        }
        return true
      }
    }
    return false
  }

  this.draw = function ()
  {
    frame.id = 'advancedWorldEdit-tab'
    frame.style.display = 'flex'

    // const toggleLightsMgr = document.createElement('button')
    // toggleLightsMgr.id = 'toggleLightsMgr'
    // toggleLightsMgr.onclick = nerthus.night.lights.give_me_the_light
    // setTip(toggleLightsMgr, 'Włącz dodatek do świateł')
    // const toggleLightsMgrImg = document.createElement('img')
    // toggleLightsMgrImg.src = '/obrazki/itemy/eve/hal18-lampa.gif'
    // toggleLightsMgr.appendChild(toggleLightsMgrImg)
    // frame.appendChild(toggleLightsMgr)

    const downloadNpcListButton = document.createElement('button')
    downloadNpcListButton.id = 'downloadNpcList'
    downloadNpcListButton.onclick = downloadNpcList
    setTip(downloadNpcListButton, 'Pobierz listę ustawień NPC')
    const downloadNpcListImg = document.createElement('img')
    downloadNpcListImg.src = '/obrazki/itemy/que/eve-porad.gif'
    downloadNpcListButton.appendChild(downloadNpcListImg)
    frame.appendChild(downloadNpcListButton)

    const addOptionsToNpcs = document.createElement('button')
    addOptionsToNpcs.id = 'addOptionsToNpcs'
    addOptionsToNpcs.onclick = addAdvancedOptionsToNpcs
    setTip(addOptionsToNpcs, 'Dodaj zaawansowane opcje do NPCów na mapie')
    const addOptionsToNpcsImg = document.createElement('img')
    addOptionsToNpcsImg.src = '/obrazki/itemy/pap/don-k.gif'
    addOptionsToNpcs.appendChild(addOptionsToNpcsImg)
    frame.appendChild(addOptionsToNpcs)

    const purgeNpcListButton = document.createElement('button')
    purgeNpcListButton.id = 'purgeNpcList'
    purgeNpcListButton.onclick = purgeNpcList
    setTip(purgeNpcListButton, 'Usuń całą listę Npców (np. by zacząć ustawiać je na innej mapie).<br>By załadować wcześniejsze grafiki z czatu do listy odśwież stronę')
    const purgeNpcListImg = document.createElement('img')
    purgeNpcListImg.src = '/obrazki/itemy/neu/tuz192.gif'
    purgeNpcListButton.appendChild(purgeNpcListImg)
    frame.appendChild(purgeNpcListButton)

    mainModuleSpace.appendChild(frame)
  }

  this.init = function ()
  {
    console.log('init')
    console.log(npcList)
    npcList = loadNpcList(false)

    window.narratorTools.advancedWorldEdit = {
      testTalkWindow: testTalkWindow,
      editTalkWindow: editTalkWindow,
      npcList: npcList
    }

    console.log(npcList)

    addToChatChecklist('*hide', parseHide)
    addToChatChecklist('*addGraf', parseAddGraf)
    addToChatChecklist('*delGraf', parseDelGraf)

    const navButton = document.createElement('button')
    navButton.id = 'open-advanced'
    navButton.style.display = 'none'
    setTip(navButton, 'Zaawansowane narzędzia WorldEdit')
    navButton.setAttribute('data-tip', 'Zaawansowane narzędzia WorldEdit <b>(Nie działają na NI!)</b>')
    navButton.addEventListener('click', displayModule.bind(this, this.name))

    const navImage = document.createElement('img')
    navImage.src = 'https://micc.garmory-cdn.cloud/obrazki/itemy/que/swider.gif'
    navButton.appendChild(navImage)

    return navButton
  }
}()

/*

//this on load:
window.narratorTools.npcList = narratorTools.advancedWorldEdit.loadNpcList(false);

narratorTools.removeNpcFromList = function (cmd)
{
    let index = narratorTools.npcList.findIndex(function (elm)
    {
        if (elm["x"] === cmd[0] && elm["y"] === cmd[1])
        {
            return true
        }
    });
    if (index > -1)
    {
        narratorTools.npcList.splice(index, 1);
    }
    narratorTools.advancedWorldEdit.saveNpcList(false);
};

/* advancedWorldEdit *//*
/*
     dialog:
        [
            główna 1
            [opcja1,dokąd]
            [opcja2,dokąd]
        ],
        [
            główna2
            [opcja1,dokąd]
            [opcja2,dokąd]

        ]

 */
// narratorTools.advancedWorldEdit.editTalk("Mag",8,7,[["hej",["khe",1],["koniec","END"]],["pa",["pa","END"],["koniec","END"]]])
/*
narratorTools.advancedWorldEdit.

/*
     dialog:
        [
            główna 1
            [opcja1,dokąd]
            [opcja2,dokąd]
        ],
        [
            główna2
            [opcja1,dokąd]
            [opcja2,dokąd]

        ]

*/
/*
narratorTools.advancedWorldEdit.parseDialogTree = function (unparsedDialog)
{
    let parsedDialog = [];
    let amountOfTabs = Object.keys(unparsedDialog).length;
    for (let i = 0; i < amountOfTabs; i++)
    {
        let response = [];
        let amountOfResponses = unparsedDialog[i.toString()].length;
        response.push(unparsedDialog[i.toString()][0]);
        for (let j = 1; j < amountOfResponses; j++)
        {
            let arr = /(.*)(?: ->)(.*)/g.exec(unparsedDialog[i.toString()][j]);
            response.push([arr[1], arr[2]]); //[response text, where to go]
        }
        parsedDialog.push(response);
    }
    return parsedDialog;
};

narratorTools.advancedWorldEdit.drawNpcDialogTreeEditor = function (npcNr)
{
    let menu = document.getElementById("menuBackground");
    let dialog = narratorTools.advancedWorldEdit.parseDialogTree(narratorTools.npcList[npcNr].dialog);
    console.log(dialog);
    let amountOfTabs = dialog.length;
    for (let i = 0; i < amountOfTabs; i++)
    {
        let tabObj = document.createElement("div");
        tabObj.id = i.toString();
        tabObj.setAttribute("tip", dialog[i][0]);
        tabObj.style.width = "20px";
        tabObj.style.height = "20px";
        tabObj.style.border = "2px solid white";

        tabObj.style.position = "absolute";
        tabObj.style.top = (i * 100 + 33).toString() + "px";
        tabObj.style.left = (i * 50).toString() + "px";
        console.log(tabObj);
        menu.appendChild(tabObj);
        let amountOfResponses = dialog[i].length;
        for (let j = 1; j < amountOfResponses; j++)
        {
            let svg = document.createElement("svg");

            menu.appendChild(svg);
            //response.push([arr[1],arr[2]]); //[response text, where to go]
        }
    }

};

narratorTools.advancedWorldEdit.

 */
